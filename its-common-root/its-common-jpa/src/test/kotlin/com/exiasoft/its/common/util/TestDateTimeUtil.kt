package com.exiasoft.its.common.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junitpioneer.jupiter.DefaultTimeZone
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId

class TestDateTimeUtil {

    @Test
    @DefaultTimeZone("Asia/Tokyo")
    fun testInstantToOffsetDateTime() {
        val instant = Instant.parse("2017-02-03T10:37:30Z")
        assertEquals("2017-02-03T18:37:30+08:00", DateTimeUtil.instant2OffsetDateTime(instant, ZoneId.of("Asia/Shanghai")).toString())
        assertEquals("2017-02-03T19:37:30+09:00", DateTimeUtil.instant2OffsetDateTime(instant).toString())
    }

    @Test
    fun testOffsetDateTimeToInstant() {
        val offsetDateTime = OffsetDateTime.parse("2017-02-03T18:37:30+08:00")
        val instant = DateTimeUtil.offsetDateTime2Instant(offsetDateTime)
        assertEquals("2017-02-03T10:37:30Z", instant.toString())
    }

}