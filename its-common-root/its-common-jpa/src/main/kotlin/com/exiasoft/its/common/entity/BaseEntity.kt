package com.exiasoft.its.common.entity

import com.exiasoft.its.common.entity.converter.InstantConverter
import java.time.Instant
import javax.persistence.*

@MappedSuperclass
abstract class BaseEntity {

    @Id
    @Column(name = "RECORD_ID")
    lateinit var id: String

    @Column(name = "CREATED_BY")
    lateinit var createdBy: String

    @Column(name = "CREATED_DATETIME")
    @Convert(converter = InstantConverter::class)
    lateinit var createdDateTime: Instant

    @Column(name = "UPDATED_BY")
    lateinit var updatedBy: String

    @Column(name = "UPDATED_DATETIME")
    @Convert(converter = InstantConverter::class)
    lateinit var updatedDateTime: Instant

    @Version
    @Column(name = "VERSION")
    var version: Long = 0

    fun isNew(): Boolean {
        return !this::id.isInitialized
    }
}