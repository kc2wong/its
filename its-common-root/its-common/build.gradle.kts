import org.jetbrains.kotlin.cli.jvm.compiler.createSourceFilesFromSourceRoots
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.jfrog.bintray.gradle.BintrayExtension


plugins {
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	id("maven")
	id("maven-publish")
	id("com.jfrog.bintray") version "1.8.5"
	kotlin("jvm") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"
}

group = "com.exiasoft.its"
version = "0.0.1-SNAPSHOT"

repositories {
	mavenLocal()
	mavenCentral()
	maven {
		setUrl("https://dl.bintray.com/kc2wong/its")
	}
}

java {
	sourceCompatibility = JavaVersion.VERSION_11
	withSourcesJar()
}

dependencies {
	val springBootVersion = "2.3.4.RELEASE"
	implementation("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	implementation("com.auth0:java-jwt:3.11.0")

	testImplementation("org.junit-pioneer:junit-pioneer:1.0.0")
	testImplementation("org.springframework.boot:spring-boot-starter-test:$springBootVersion") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.getByName<Task>("install") {
	dependsOn("kotlinSourcesJar")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

val archives by configurations

configure<PublishingExtension> {
	publications {
		create<MavenPublication>("maven") {
			setArtifacts(archives.artifacts)
		}
	}

	repositories {
		maven {
			setUrl("https://dl.bintray.com/kc2wong/its")
		}
	}
}

configure<BintrayExtension> {

	user = "kc2wong"
	key = "d6c46def929b8b3c961166fedfeb9e14e199e635"
	setPublications("maven")
	publish = true

	pkg.apply {
		repo = "its"
		name = project.name
		vcsUrl = "https://gitlab.com/its9/its-common.git"
		setLicenses("MIT")
	}
}