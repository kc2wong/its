package com.exiasoft.its.common.bean

import com.exiasoft.its.common.domain.UserProfile

open class RequestData {
    lateinit var token: String
    lateinit var userProfile: UserProfile
}
