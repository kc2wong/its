package com.exiasoft.its.common.endpoint.rest

import com.exiasoft.its.common.endpoint.rest.dto.ErrorResponseDto
import com.exiasoft.its.common.exception.SystemException
import com.exiasoft.its.common.exception.ValidationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class ControllerExceptionHandler {

    @ExceptionHandler(value = [ValidationException::class])
    fun handleValidateException(ex: ValidationException, request: WebRequest) : ResponseEntity<ErrorResponseDto> {
        val errorParamList = ex.errorParam.toList()
        val errorResponseDto = ErrorResponseDto(ex.errorCode, errorParamList, ex.message)
        return ResponseEntity.status(ex.getHttpStatus()).body(errorResponseDto)
    }

    @ExceptionHandler(value = [SystemException::class])
    fun handleSystemException(ex: SystemException, request: WebRequest) : ResponseEntity<ErrorResponseDto> {
        val errorParamList = ex.errorParam.toList()
        val errorResponseDto = ErrorResponseDto(ex.errorCode, errorParamList, ex.message)
        return ResponseEntity.status(ex.getHttpStatus()).body(errorResponseDto)
    }

}