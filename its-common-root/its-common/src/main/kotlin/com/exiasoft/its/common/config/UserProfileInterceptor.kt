package com.exiasoft.its.common.config

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.exiasoft.its.common.bean.RequestData
import com.exiasoft.its.common.domain.UserProfile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.web.servlet.HandlerInterceptor
import java.nio.charset.Charset
import java.security.KeyFactory
import java.security.interfaces.RSAPublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.annotation.PostConstruct
import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class UserProfileInterceptor() : HandlerInterceptor {

    private val logger: Logger = LoggerFactory.getLogger(UserProfileInterceptor::class.java)

    @Resource
    lateinit var requestData: RequestData

    @Autowired
    lateinit var applicationContext: ApplicationContext

    @Value("\${jwt.key.public}")
    lateinit var publicKeyPath: String

    lateinit var publicKey: RSAPublicKey
    lateinit var verifyAlgorithm: Algorithm

    @PostConstruct
    fun init() {
        applicationContext.getResource(publicKeyPath).inputStream.use {
            val publicKeyPem = String(it.readAllBytes(), Charset.defaultCharset())
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace(System.lineSeparator(), "")
                    .replace("-----END PUBLIC KEY-----", "")

            val src = Base64.getDecoder().decode(publicKeyPem)
            publicKey = KeyFactory.getInstance("RSA").generatePublic(X509EncodedKeySpec(src)) as RSAPublicKey
        }
        verifyAlgorithm = Algorithm.RSA256(publicKey, null)
    }

    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        logger.info("UserProfileInterceptor.preHandle(), requestData = {}", requestData)
        val headerNames = request.headerNames.toList()
        if (logger.isDebugEnabled) {
            logger.debug("header names = $headerNames")
        }
        val userProfile = UserProfile()
        return request.headerNames.toList().stream()
                .filter { it.toLowerCase() == "authorization" }
                .findFirst().map {
                    val verifier = JWT.require(verifyAlgorithm).build()

                    var infoGotFromJwt = false
                    try {
                        val headerValue = request.getHeader(it)
                        val jwt = if (headerValue.toLowerCase().startsWith("bearer ")) headerValue.substring(7) else headerValue
                        logger.debug("jwt = $jwt")
                        with(verifier.verify(jwt), {
                            if (!this.getClaim("name").isNull && !this.getClaim("email").isNull) {
                                userProfile.userid = this.subject
                                userProfile.name = this.getClaim("name").asString()
                                userProfile.email = this.getClaim("email").asString()
                                infoGotFromJwt = true
                            }
                        })
                    } catch (ex: JWTVerificationException) {
                        logger.warn("Failed to verify request JWT", ex)
                    }
                    if (!infoGotFromJwt) {
                        userProfile.userid = "system"
                        userProfile.name = "ITS System User"
                    }
                    requestData.userProfile = userProfile

                    true
                }
                .orElseGet {
                    userProfile.userid = "system"
                    userProfile.name = "ITS System User"
                    requestData.userProfile = userProfile
                    super.preHandle(request, response, handler)
                }
    }
}