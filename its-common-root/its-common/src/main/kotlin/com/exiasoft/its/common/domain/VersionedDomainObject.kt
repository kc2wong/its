package com.exiasoft.its.common.domain

import java.time.Instant

interface VersionedDomainObject {
    var createdBy: String
    var createdDateTime: Instant
    var updatedBy: String
    var updatedDateTime: Instant
    var version: Long
}