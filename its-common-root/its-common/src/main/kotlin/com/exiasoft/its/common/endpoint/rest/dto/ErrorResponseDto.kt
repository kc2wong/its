package com.exiasoft.its.common.endpoint.rest.dto

class ErrorResponseDto(val errorCode: String, val errorParam: List<String>, val errorMessage: String) {
}