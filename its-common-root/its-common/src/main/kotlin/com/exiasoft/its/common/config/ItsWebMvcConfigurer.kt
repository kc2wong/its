package com.exiasoft.its.common.config

import com.exiasoft.its.common.bean.RequestData
import com.exiasoft.its.common.endpoint.rest.dto.ErrorResponseDto
import com.exiasoft.its.common.exception.SystemException
import com.exiasoft.its.common.exception.ValidationException
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.client.ClientHttpResponse
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.*
import org.springframework.web.context.annotation.RequestScope
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.net.URI

@Configuration
@ConditionalOnClass(value = [RestController::class])
@ComponentScan(value = ["com.exiasoft.itscommon.endpoint.rest"])
class ItsWebMvcConfigurer : WebMvcConfigurer {

    override fun addInterceptors(registry: InterceptorRegistry) {
        super.addInterceptors(registry)
        registry.addInterceptor(provideUserProfileInterceptor()).addPathPatterns("/**")
    }

    @Bean
    fun provideUserProfileInterceptor() : UserProfileInterceptor {
        return UserProfileInterceptor()
    }

    @Bean
    @RequestScope
    fun provideRequestData() : RequestData {
        return RequestData()
    }

    @Bean
    fun provideRestTemplate() : RestTemplate {
        return RestTemplateBuilder().errorHandler(ErrorResponseHandler()).build()
    }

    class ErrorResponseHandler : DefaultResponseErrorHandler() {
        private val logger = LoggerFactory.getLogger(ErrorResponseHandler::class.java)

        override fun handleError(response: ClientHttpResponse, httpStatus: HttpStatus) {
            val objectMapper = ObjectMapper()
            try {
                super.handleError(response, httpStatus)
            }
            catch (httpClientErrorException : HttpClientErrorException) {
                val responseBody = httpClientErrorException.responseBodyAsString
                try {
                    val errorResponseDto = objectMapper.readValue<ErrorResponseDto>(responseBody, ErrorResponseDto::class.java)
                    throw ValidationException(errorResponseDto.errorCode, errorResponseDto.errorParam, errorResponseDto.errorMessage)
                }
                catch (ex: Exception) {
                    logger.warn("Cannot deserialize error response \"responseBody\" to ErrorResponseDto", ex)
                    throw httpClientErrorException
                }
            }
            catch (httpServerErrorException : HttpServerErrorException) {
                val responseBody = httpServerErrorException.responseBodyAsString
                try {
                    val errorResponseDto = objectMapper.readValue<ErrorResponseDto>(responseBody, ErrorResponseDto::class.java)
                    throw SystemException(errorResponseDto.errorCode, errorResponseDto.errorParam, errorResponseDto.errorMessage)
                }
                catch (ex: Exception) {
                    logger.warn("Cannot deserialize error response \"responseBody\" to ErrorResponseDto", ex)
                    throw httpServerErrorException
                }
            }
        }
    }
}