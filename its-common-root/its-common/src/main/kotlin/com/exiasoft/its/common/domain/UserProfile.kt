package com.exiasoft.its.common.domain

class UserProfile() {
    lateinit var userid: String
    lateinit var name: String
    var email: String? = null
}