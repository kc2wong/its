package com.exiasoft.its.common.util

class StringUtil {
    companion object {
        const val EMPTY_STRING = ""

        fun ensureMaxLength(str: String, maxLenth: Int) : String {
            return if (str.length > maxLenth) str.substring(0, maxLenth) else str
        }
    }
}