package com.exiasoft.its.referencedata.entity.repo

import com.exiasoft.its.referencedata.entity.CurrencyEntity
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository


@Repository
interface CurrencyRepository : JpaRepository<CurrencyEntity, String>, JpaSpecificationExecutor<CurrencyEntity> {

    @EntityGraph(value = "currency-locale-graph", type = EntityGraph.EntityGraphType.LOAD)
    fun findByCode(code: String): CurrencyEntity?

    @EntityGraph(value = "currency-locale-graph", type = EntityGraph.EntityGraphType.LOAD)
    fun findByIdIn(id: List<String>): List<CurrencyEntity>

}
