package com.exiasoft.its.referencedata.endpoint.rest.api

import com.exiasoft.its.common.bean.RequestData
import com.exiasoft.its.common.exception.ValidationException
import com.exiasoft.its.referencedata.endpoint.rest.mapper.CurrencyDtoMapper
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto
import com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto
import com.exiasoft.its.referencedata.endpoint.rest.model.PageOfCurrencyDto
import com.exiasoft.its.referencedata.service.CurrencyService
import com.exiasoft.its.referencedata.service.exception.ErrorCode
import org.mapstruct.factory.Mappers
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import kotlin.streams.toList

@Component
class CurrencyApiDelegateImpl(@Value("\${application.defaultPageSize}") val defaultPageSize: Int, @Autowired val currencyService: CurrencyService) : CurrencyApiDelegate {

    private val logger = LoggerFactory.getLogger(CurrencyApiDelegateImpl::class.java)
    private val mapper: CurrencyDtoMapper = Mappers.getMapper(CurrencyDtoMapper::class.java)

    @Autowired
    lateinit var requestData: RequestData

    override fun createCurrency(createCurrencyRequestDto: CreateCurrencyRequestDto): ResponseEntity<CurrencyDto> {
        val newCurrency = currencyService.createCurrency(mapper.dto2Domain(createCurrencyRequestDto), requestData.userProfile)
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.domain2Dto(newCurrency))
    }

    override fun findCurrency(currencyCode: String?, name: String?, shortName: String?, offset: Int?, limit: Int?, sort: String?): ResponseEntity<PageOfCurrencyDto> {
        val searchResult = currencyService.findCurrency(currencyCode, name, shortName, offset, limit ?: defaultPageSize, sort)
        val response = PageOfCurrencyDto()
        response.data = searchResult.get().map { mapper.domain2Dto(it) }.toList()
        response.start = searchResult.pageable.offset.toInt()
        response.end = response.start + response.data.size
        response.total = searchResult.totalElements.toInt()
        return ResponseEntity.ok().body(response)
    }

    override fun getCurrency(currencyCode: String): ResponseEntity<CurrencyDto> {
        return currencyService.getCurrency(currencyCode) ?.let {
            ResponseEntity.ok().body(mapper.domain2Dto(it))
        } ?: throw ValidationException(ErrorCode.CURRENCY_NOT_FOUND, listOf(currencyCode))
    }

    override fun putCurrency(currencyCode: String, currencyDto: CurrencyDto): ResponseEntity<CurrencyDto> {
        val updatedCurrency = currencyService.updateCurrency(currencyCode, mapper.dto2Domain(currencyDto), requestData.userProfile)
        return ResponseEntity.ok(mapper.domain2Dto(updatedCurrency))
    }
}