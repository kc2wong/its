package com.exiasoft.its.referencedata.service.model

import com.exiasoft.its.common.domain.Locale
import java.time.Instant

class Currency {

    lateinit var code: String

    var decimalPlace: Int = 0

    var shortName: Map<Locale, String> = emptyMap()

    var name: Map<Locale, String> = emptyMap()

    lateinit var createdBy: String

    lateinit var createdDateTime: Instant

    lateinit var updatedBy: String

    lateinit var updatedDateTime: Instant

    var version: Long = 0


}