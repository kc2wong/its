package com.exiasoft.its.referencedata.endpoint.rest.mapper

import com.exiasoft.its.common.domain.Locale
import com.exiasoft.its.common.endpoint.rest.mapper.BaseDtoMapper
import com.exiasoft.its.common.util.EnumUtil
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest
import com.exiasoft.its.referencedata.service.model.Currency
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestNameDto
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestShortNameDto
import com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(uses = [BaseDtoMapper::class])
interface CurrencyDtoMapper {

    @Mappings(value = [
        Mapping(source = "code", target = "currencyCode"),
        Mapping(target = "name", expression = "java(Companion.toNameDto(currency.getName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toShortNameDto(currency.getShortName()))")
    ])
    fun domain2Dto(currency: Currency) : CurrencyDto

    @Mappings(value = [
        Mapping(source = "currencyCode", target = "code"),
        Mapping(target = "name", expression = "java(Companion.toNameDomain(request.getName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(request.getShortName()))")
    ])
    fun dto2Domain(request: CreateCurrencyRequestDto) : CreateCurrencyRequest

    @Mappings(value = [
        Mapping(source = "currencyCode", target = "code"),
        Mapping(target = "name", expression = "java(Companion.toNameDomain(request.getName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(request.getShortName()))"),
        Mapping(target = "createdBy", ignore = true),
        Mapping(target = "createdDateTime", ignore = true),
        Mapping(target = "updatedBy", ignore = true),
        Mapping(target = "updatedDateTime", ignore = true)
    ])
    fun dto2Domain(request: CurrencyDto) : Currency

    companion object {

        fun toNameDto(name: Map<Locale, String>) : List<CreateCurrencyRequestNameDto> {
            return name.map {
                val rtn = CreateCurrencyRequestNameDto()
                rtn.locale = it.key.value
                rtn.value = it.value
                rtn
            }.toList()
        }

        fun toShortNameDto(name: Map<Locale, String>) : List<CreateCurrencyRequestShortNameDto> {
            return name.map {
                val rtn = CreateCurrencyRequestShortNameDto()
                rtn.locale = it.key.value
                rtn.value = it.value
                rtn
            }.toList()
        }

        fun toNameDomain(name: List<CreateCurrencyRequestNameDto>) : Map<Locale, String> {
            return name.filter { EnumUtil.str2Enum(Locale::class.java, it.locale) != null }
                    .map {
                        EnumUtil.str2Enum(Locale::class.java, it.locale)!! to it.value
                    }.toMap()
        }

        fun toShortNameDomain(name: List<CreateCurrencyRequestShortNameDto>) : Map<Locale, String> {
            return name.filter { EnumUtil.str2Enum(Locale::class.java, it.locale) != null }
                    .map {
                        EnumUtil.str2Enum(Locale::class.java, it.locale)!! to it.value
                    }.toMap()
        }

    }
}