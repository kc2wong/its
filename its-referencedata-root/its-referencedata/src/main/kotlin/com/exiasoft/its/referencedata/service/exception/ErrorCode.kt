package com.exiasoft.its.referencedata.service.exception

import com.exiasoft.its.common.exception.ErrorMessage
import com.exiasoft.its.common.exception.ErrorMessageProvider


class ErrorCode: ErrorMessageProvider {
    companion object {

        @ErrorMessage(message = "Currency with code %s already exists")
        const val CURRENCY_ALREADY_EXISTS = "REFDATA.00001"

        @ErrorMessage(message = "Currency with code %s is not found")
        const val CURRENCY_NOT_FOUND = "REFDATA.00002"

        @ErrorMessage(message = "Sort criteria %s is invalid")
        const val INVALID_SORT_CRITERIA = "REFDATA.00003"

        @ErrorMessage(message = "Missing %s name or shortname")
        const val MISSING_ENGLISH_NAME = "REFDATA.00003"

    }
}