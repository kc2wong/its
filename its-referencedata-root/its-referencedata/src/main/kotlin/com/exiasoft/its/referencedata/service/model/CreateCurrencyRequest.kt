package com.exiasoft.its.referencedata.service.model

import com.exiasoft.its.common.domain.Locale

data class CreateCurrencyRequest(val code: String, val decimalPlace: Int, var shortName: Map<Locale, String>, var name: Map<Locale, String>)