package com.exiasoft.its.referencedata.config

import com.exiasoft.its.common.exception.BaseException
import com.exiasoft.its.referencedata.service.exception.ErrorCode
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class ApplicationConfig() {

    @PostConstruct
    fun init() {
        BaseException.registerMessageProvider(ErrorCode::class.java)
    }

}