package com.exiasoft.its.referencedata

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ItsReferenceDataApplication

fun main(args: Array<String>) {
	runApplication<ItsReferenceDataApplication>(*args)
}
