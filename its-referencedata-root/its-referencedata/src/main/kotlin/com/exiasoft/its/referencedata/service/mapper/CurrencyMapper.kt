package com.exiasoft.its.referencedata.service.mapper

import com.exiasoft.its.common.domain.Locale
import com.exiasoft.its.common.util.StringUtil
import com.exiasoft.its.common.util.StringUtil.Companion.EMPTY_STRING
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest
import com.exiasoft.its.referencedata.service.model.Currency
import com.exiasoft.its.referencedata.entity.CurrencyEntity
import com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings

@Mapper
interface CurrencyMapper {

    @Mappings(value = [
        Mapping(target = "name", expression = "java(Companion.toNameDomain(currencyEntity.getLocale(), currencyEntity.getName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(currencyEntity.getLocale(), currencyEntity.getShortName()))")
    ])
    fun entity2Domain(currencyEntity: CurrencyEntity) : Currency

    @Mappings(value = [
        Mapping(target = "id", ignore = true),
        Mapping(target = "name", expression = "java(Companion.toEntityName(currency.getName(), currency.getShortName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toEntityShortName(currency.getName(), currency.getShortName()))"),
        Mapping(target = "locale", expression = "java(Companion.toLocaleEntity(currency.getName(), currency.getShortName()))")
    ])
    fun domain2Entity(currency: Currency) : CurrencyEntity

    @Mappings(value = [
        Mapping(target = "id", ignore = true),
        Mapping(target = "code", ignore = true),
        Mapping(target = "name", expression = "java(Companion.toEntityName(currency.getName(), currency.getShortName()))"),
        Mapping(target = "shortName", expression = "java(Companion.toEntityShortName(currency.getName(), currency.getShortName()))"),
        Mapping(target = "createdBy", ignore = true),
        Mapping(target = "createdDateTime", ignore = true),
        Mapping(target = "locale", expression = "java(Companion.toLocaleEntity(currency.getName(), currency.getShortName()))")
    ])
    fun domain2Entity(currency: Currency, @MappingTarget currencyEntity: CurrencyEntity)

    @Mappings(value = [
        Mapping(target = "createdBy", ignore = true),
        Mapping(target = "createdDateTime", ignore = true),
        Mapping(target = "updatedBy", ignore = true),
        Mapping(target = "updatedDateTime", ignore = true),
        Mapping(target = "version", ignore = true)
    ])
    fun createDomainFromRequest(request: CreateCurrencyRequest) : Currency

    @Mappings
    fun cloneDomain(currency: Currency) : Currency

    companion object {

        fun toLocaleEntity(name: Map<Locale, String>, shortName: Map<Locale, String>): List<CurrencyLocaleEntity> {
            return (name.keys union shortName.keys).filter { it != Locale.EN }.map {
                val currencyLocaleEntity = CurrencyLocaleEntity()
                currencyLocaleEntity.locale = it
                currencyLocaleEntity.name = name[it] ?: ""
                currencyLocaleEntity.shortName = shortName[it] ?: ""
                currencyLocaleEntity
            }.toList()
        }

        fun toEntityName(name: Map<Locale, String>, shortName: Map<Locale, String>): String {
            return name.getOrDefault(Locale.EN, shortName.getOrDefault(Locale.EN, EMPTY_STRING))
        }

        fun toEntityShortName(name: Map<Locale, String>, shortName: Map<Locale, String>): String {
            return shortName.getOrDefault(Locale.EN, StringUtil.ensureMaxLength(name.getOrDefault(Locale.EN, EMPTY_STRING), 10))
        }

//        fun toLocaleEntity(locale: List<CurrencyLocaleEntity>, name: Map<Locale, String>, shortName: Map<Locale, String>): List<CurrencyLocaleEntity> {
//            val localeMap = locale.map { it.locale to it }.toMap()
//            return (name.keys union shortName.keys).map {
//                val currencyLocaleEntity = localeMap[it] ?: CurrencyLocaleEntity()
//                currencyLocaleEntity.locale = it
//                currencyLocaleEntity.name = name[it] ?: ""
//                currencyLocaleEntity.shortName = shortName[it] ?: ""
//                currencyLocaleEntity
//            }.toList()
//        }

        fun toNameDomain(locale: List<CurrencyLocaleEntity>, enName: String): Map<Locale, String> {
            return locale.map {
                it.locale to it.name
            }.toMap().plus(Pair(Locale.EN, enName))
        }

        fun toShortNameDomain(locale: List<CurrencyLocaleEntity>, enShortName: String): Map<Locale, String> {
            return locale.map {
                it.locale to it.shortName
            }.toMap().plus(Pair(Locale.EN, enShortName))
        }

    }

}