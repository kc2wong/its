package com.exiasoft.its.referencedata.service.impl

import com.exiasoft.its.common.domain.Locale
import com.exiasoft.its.common.domain.UserProfile
import com.exiasoft.its.common.entity.SearchCriteria
import com.exiasoft.its.common.exception.ValidationException
import com.exiasoft.its.common.util.DateTimeUtil
import com.exiasoft.its.common.util.JpaUtil
import com.exiasoft.its.common.util.StringUtil
import com.exiasoft.its.common.util.StringUtil.Companion.EMPTY_STRING
import com.exiasoft.its.referencedata.entity.CurrencyEntity
import com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity
import com.exiasoft.its.referencedata.entity.repo.CurrencyRepository
import com.exiasoft.its.referencedata.service.CurrencyService
import com.exiasoft.its.referencedata.service.exception.ErrorCode
import com.exiasoft.its.referencedata.service.exception.ErrorCode.Companion.CURRENCY_ALREADY_EXISTS
import com.exiasoft.its.referencedata.service.exception.ErrorCode.Companion.CURRENCY_NOT_FOUND
import com.exiasoft.its.referencedata.service.exception.ErrorCode.Companion.MISSING_ENGLISH_NAME
import com.exiasoft.its.referencedata.service.mapper.CurrencyMapper
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest
import com.exiasoft.its.referencedata.service.model.Currency
import org.mapstruct.factory.Mappers
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class CurrencyServiceImpl(@Autowired val currencyRepository: CurrencyRepository) : CurrencyService {

    private val mapper: CurrencyMapper = Mappers.getMapper(CurrencyMapper::class.java)
    private val logger = LoggerFactory.getLogger(CurrencyServiceImpl::class.java)

    @Transactional
    override fun createCurrency(createCurrencyRequest: CreateCurrencyRequest, invoker: UserProfile): Currency {
        if (!createCurrencyRequest.shortName.containsKey(Locale.EN) || !createCurrencyRequest.name.containsKey(Locale.EN)) {
            throw ValidationException(MISSING_ENGLISH_NAME, listOf(Locale.EN.value))
        }
        currencyRepository.findByCode(createCurrencyRequest.code) ?.let {
            throw ValidationException(CURRENCY_ALREADY_EXISTS, listOf(createCurrencyRequest.code))
        }

        val currency = mapper.createDomainFromRequest(createCurrencyRequest)
        enrichName(currency.name, currency.shortName).let {
            currency.name = it.first
            currency.shortName = it.second
        }

        val now = DateTimeUtil.currentTime()
        currency.createdBy = invoker.userid
        currency.createdDateTime = now
        currency.updatedBy = invoker.userid
        currency.updatedDateTime = now
        currency.version = 1

        val currencyEntity = mapper.domain2Entity(currency)
        currencyEntity.id = UUID.randomUUID().toString()
        currencyEntity.locale.forEach {
            it.id = UUID.randomUUID().toString()
            it.createdBy = currency.createdBy
            it.createdDateTime = currency.createdDateTime
            it.updatedBy = currency.updatedBy
            it.updatedDateTime = currency.updatedDateTime
            it.version = currency.version
            it.currency = currencyEntity
        }

        currencyRepository.save(currencyEntity)
        return mapper.entity2Domain(currencyEntity)
    }

    @Transactional
    override fun updateCurrency(currencyCode: String, currency: Currency, invoker: UserProfile): Currency {
        val currencyEntity = currencyRepository.findByCode(currencyCode)
                ?: throw ValidationException(CURRENCY_NOT_FOUND, listOf(currencyCode))

        // Clone input currency for name enrichment
        val newCurrency = mapper.cloneDomain(currency)
        newCurrency.code = currencyCode
        enrichName(newCurrency.name, newCurrency.shortName).let {
            newCurrency.name = it.first
            newCurrency.shortName = it.second
        }
        newCurrency.updatedBy = invoker.userid
        newCurrency.updatedDateTime = DateTimeUtil.currentTime()

        // Map domain object to entity
        mapper.domain2Entity(newCurrency, currencyEntity)
        with(currencyEntity, {
            // Remove locales that are not found in new image
            val newLocales = newCurrency.name.keys
            this.locale
                    .filter { !newLocales.contains(it.locale) }
                    .forEach {
                        it.currency = null
                        this.locale.remove(it)
                    }

            this.locale.forEach {
                if (it.isNew()) {
                    it.id = UUID.randomUUID().toString()
                    it.createdBy = this.createdBy
                    it.createdDateTime = this.createdDateTime
                    it.currency = this
                }
                it.updatedBy = this.updatedBy
                it.updatedDateTime = this.updatedDateTime
                it.version = this.version
            }
        })

        // save entity to db
        currencyRepository.save(currencyEntity)
        return mapper.entity2Domain(currencyEntity)
    }

    override fun getCurrency(code: String): Currency? {
        return currencyRepository.findByCode(code) ?.let {
            mapper.entity2Domain(it)
        }
    }

    override fun findCurrency(code: String?, name: String?, shortName: String?, offset: Int?, limit: Int, sort: String?): Page<Currency> {
        logger.info("findCurrency(), code = {}, name = {}, sortName = {}", code, name, shortName)
        val validSortCriteria = setOf("code", "name", "shortName", "createdBy", "createdDateTime", "updatedBy", "updatedDateTime")
        var sortCriteria = JpaUtil.toSortCriteria(sort ?: "code")
        for (idx in 0 until sortCriteria.count()) {
            with(sortCriteria.elementAt(idx).property, {
                if (!validSortCriteria.contains(this)) {
                    throw ValidationException(ErrorCode.INVALID_SORT_CRITERIA, listOf(this))
                }
            })
        }
        // Append default sort criteria
        if (sortCriteria.getOrderFor("code") == null) {
            sortCriteria = sortCriteria.and(Sort.by("code").ascending())
        }

        val specification = mutableListOf<Specification<CurrencyEntity>>()
        code ?.let {
            specification.add(JpaUtil.withCriteria<CurrencyEntity, String>(SearchCriteria("code", SearchCriteria.Condition.EQUAL, it)))
        }
        name ?.let {
            specification.add(JpaUtil.or(listOf(
                    JpaUtil.withCriteria<CurrencyEntity, String>(SearchCriteria("name", SearchCriteria.Condition.LIKE, it)),
                    JpaUtil.withOuterJoinCriteria<CurrencyEntity, CurrencyLocaleEntity, String>("locale", SearchCriteria("name", SearchCriteria.Condition.LIKE, it))
            )))
        }
        shortName ?.let {
            specification.add(JpaUtil.or(listOf(
                    JpaUtil.withCriteria<CurrencyEntity, String>(SearchCriteria("shortName", SearchCriteria.Condition.LIKE, it)),
                    JpaUtil.withOuterJoinCriteria<CurrencyEntity, CurrencyLocaleEntity, String>("locale", SearchCriteria("shortName", SearchCriteria.Condition.LIKE, it))
            )))
        }

        val pageRequest = PageRequest.of(offset?:0 / limit, limit, sortCriteria)
        val searchResult = if (specification.isEmpty()) currencyRepository.findAll(pageRequest) else currencyRepository.findAll(JpaUtil.and(specification), pageRequest)
        val detailMap = currencyRepository.findByIdIn(searchResult.content.map { it.id }.toList())
                .map { it.id to it }.toMap()

        return searchResult.map {
            mapper.entity2Domain(detailMap[it.id] ?: it)
        }
    }

    private fun enrichName(nameMap: Map<Locale, String>, shortNameMap: Map<Locale, String>) : Pair<Map<Locale, String>, Map<Locale, String>> {
        val languages = nameMap.keys union shortNameMap.keys
        val newNameMap = languages.map {
            it to (nameMap[it] ?: shortNameMap[it] ?: EMPTY_STRING)
        }.toMap()
        val newShortNameMap = languages.map {
            it to (shortNameMap[it] ?: (StringUtil.ensureMaxLength(shortNameMap[it] ?: EMPTY_STRING, 10)))
        }.toMap()
        return Pair(newNameMap, newShortNameMap)
    }

}