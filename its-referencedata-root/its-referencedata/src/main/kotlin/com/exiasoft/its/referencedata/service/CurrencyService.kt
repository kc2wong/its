package com.exiasoft.its.referencedata.service

import com.exiasoft.its.common.domain.UserProfile
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest
import com.exiasoft.its.referencedata.service.model.Currency
import org.springframework.data.domain.Page

interface CurrencyService {

    fun createCurrency(createCurrencyRequest: CreateCurrencyRequest, invoker: UserProfile): Currency
    fun updateCurrency(currencyCode: String, currency: Currency, invoker: UserProfile): Currency
    fun getCurrency(code: String): Currency?
    fun findCurrency(code: String?, name: String?, shortName: String?, offset: Int?, limit: Int, sort: String?): Page<Currency>

}