package com.exiasoft.its.referencedata.service.mapper;

import com.exiasoft.its.common.domain.Locale;
import com.exiasoft.its.referencedata.entity.CurrencyEntity;
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest;
import com.exiasoft.its.referencedata.service.model.Currency;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-29T11:50:11+0800",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from kotlin-annotation-processing-gradle-1.3.72.jar, environment: Java 11.0.8 (Oracle Corporation)"
)
public class CurrencyMapperImpl implements CurrencyMapper {

    @Override
    public Currency entity2Domain(CurrencyEntity currencyEntity) {
        if ( currencyEntity == null ) {
            return null;
        }

        Currency currency = new Currency();

        currency.setCode( currencyEntity.getCode() );
        currency.setDecimalPlace( currencyEntity.getDecimalPlace() );
        currency.setCreatedBy( currencyEntity.getCreatedBy() );
        currency.setCreatedDateTime( currencyEntity.getCreatedDateTime() );
        currency.setUpdatedBy( currencyEntity.getUpdatedBy() );
        currency.setUpdatedDateTime( currencyEntity.getUpdatedDateTime() );
        currency.setVersion( currencyEntity.getVersion() );

        currency.setName( Companion.toNameDomain(currencyEntity.getLocale(), currencyEntity.getName()) );
        currency.setShortName( Companion.toShortNameDomain(currencyEntity.getLocale(), currencyEntity.getShortName()) );

        return currency;
    }

    @Override
    public CurrencyEntity domain2Entity(Currency currency) {
        if ( currency == null ) {
            return null;
        }

        CurrencyEntity currencyEntity = new CurrencyEntity();

        currencyEntity.setCreatedBy( currency.getCreatedBy() );
        currencyEntity.setCreatedDateTime( currency.getCreatedDateTime() );
        currencyEntity.setUpdatedBy( currency.getUpdatedBy() );
        currencyEntity.setUpdatedDateTime( currency.getUpdatedDateTime() );
        currencyEntity.setVersion( currency.getVersion() );
        currencyEntity.setCode( currency.getCode() );
        currencyEntity.setDecimalPlace( currency.getDecimalPlace() );

        currencyEntity.setName( Companion.toEntityName(currency.getName(), currency.getShortName()) );
        currencyEntity.setShortName( Companion.toEntityShortName(currency.getName(), currency.getShortName()) );
        currencyEntity.setLocale( Companion.toLocaleEntity(currency.getName(), currency.getShortName()) );

        return currencyEntity;
    }

    @Override
    public void domain2Entity(Currency currency, CurrencyEntity currencyEntity) {
        if ( currency == null ) {
            return;
        }

        currencyEntity.setUpdatedBy( currency.getUpdatedBy() );
        currencyEntity.setUpdatedDateTime( currency.getUpdatedDateTime() );
        currencyEntity.setVersion( currency.getVersion() );
        currencyEntity.setDecimalPlace( currency.getDecimalPlace() );

        currencyEntity.setName( Companion.toEntityName(currency.getName(), currency.getShortName()) );
        currencyEntity.setShortName( Companion.toEntityShortName(currency.getName(), currency.getShortName()) );
        currencyEntity.setLocale( Companion.toLocaleEntity(currency.getName(), currency.getShortName()) );
    }

    @Override
    public Currency createDomainFromRequest(CreateCurrencyRequest request) {
        if ( request == null ) {
            return null;
        }

        Currency currency = new Currency();

        currency.setCode( request.getCode() );
        currency.setDecimalPlace( request.getDecimalPlace() );
        Map<Locale, String> map = request.getShortName();
        if ( map != null ) {
            currency.setShortName( new HashMap<Locale, String>( map ) );
        }
        Map<Locale, String> map1 = request.getName();
        if ( map1 != null ) {
            currency.setName( new HashMap<Locale, String>( map1 ) );
        }

        return currency;
    }

    @Override
    public Currency cloneDomain(Currency currency) {
        if ( currency == null ) {
            return null;
        }

        Currency currency1 = new Currency();

        currency1.setCode( currency.getCode() );
        currency1.setDecimalPlace( currency.getDecimalPlace() );
        Map<Locale, String> map = currency.getShortName();
        if ( map != null ) {
            currency1.setShortName( new HashMap<Locale, String>( map ) );
        }
        Map<Locale, String> map1 = currency.getName();
        if ( map1 != null ) {
            currency1.setName( new HashMap<Locale, String>( map1 ) );
        }
        currency1.setCreatedBy( currency.getCreatedBy() );
        currency1.setCreatedDateTime( currency.getCreatedDateTime() );
        currency1.setUpdatedBy( currency.getUpdatedBy() );
        currency1.setUpdatedDateTime( currency.getUpdatedDateTime() );
        currency1.setVersion( currency.getVersion() );

        return currency1;
    }
}
