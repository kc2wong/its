package com.exiasoft.its.referencedata.endpoint.rest.mapper;

import com.exiasoft.its.common.domain.Locale;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto;
import com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest;
import com.exiasoft.its.referencedata.service.model.Currency;
import java.util.Map;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-29T11:55:10+0800",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from kotlin-annotation-processing-gradle-1.3.72.jar, environment: Java 11.0.8 (Oracle Corporation)"
)
public class CurrencyDtoMapperImpl implements CurrencyDtoMapper {

    @Override
    public CurrencyDto domain2Dto(Currency currency) {
        if ( currency == null ) {
            return null;
        }

        CurrencyDto currencyDto = new CurrencyDto();

        currencyDto.setCurrencyCode( currency.getCode() );
        currencyDto.setDecimalPlace( currency.getDecimalPlace() );
        currencyDto.setVersion( currency.getVersion() );

        currencyDto.setName( Companion.toNameDto(currency.getName()) );
        currencyDto.setShortName( Companion.toShortNameDto(currency.getShortName()) );

        return currencyDto;
    }

    @Override
    public CreateCurrencyRequest dto2Domain(CreateCurrencyRequestDto request) {
        if ( request == null ) {
            return null;
        }

        String code = null;
        int decimalPlace = 0;

        code = request.getCurrencyCode();
        if ( request.getDecimalPlace() != null ) {
            decimalPlace = request.getDecimalPlace();
        }

        Map<Locale, String> name = Companion.toNameDomain(request.getName());
        Map<Locale, String> shortName = Companion.toShortNameDomain(request.getShortName());

        CreateCurrencyRequest createCurrencyRequest = new CreateCurrencyRequest( code, decimalPlace, shortName, name );

        return createCurrencyRequest;
    }

    @Override
    public Currency dto2Domain(CurrencyDto request) {
        if ( request == null ) {
            return null;
        }

        Currency currency = new Currency();

        currency.setCode( request.getCurrencyCode() );
        if ( request.getDecimalPlace() != null ) {
            currency.setDecimalPlace( request.getDecimalPlace() );
        }
        if ( request.getVersion() != null ) {
            currency.setVersion( request.getVersion() );
        }

        currency.setName( Companion.toNameDomain(request.getName()) );
        currency.setShortName( Companion.toShortNameDomain(request.getShortName()) );

        return currency;
    }
}
