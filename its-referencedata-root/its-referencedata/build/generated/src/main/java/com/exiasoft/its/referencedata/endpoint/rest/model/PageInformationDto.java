package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PageInformationDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public class PageInformationDto   {
  @JsonProperty("start")
  private Integer start;

  @JsonProperty("end")
  private Integer end;

  @JsonProperty("total")
  private Integer total;

  public PageInformationDto start(Integer start) {
    this.start = start;
    return this;
  }

  /**
   * Starting record number (from 1) in matched result
   * @return start
  */
  @ApiModelProperty(example = "26", value = "Starting record number (from 1) in matched result")


  public Integer getStart() {
    return start;
  }

  public void setStart(Integer start) {
    this.start = start;
  }

  public PageInformationDto end(Integer end) {
    this.end = end;
    return this;
  }

  /**
   * Ending record number in matched result
   * @return end
  */
  @ApiModelProperty(example = "50", value = "Ending record number in matched result")


  public Integer getEnd() {
    return end;
  }

  public void setEnd(Integer end) {
    this.end = end;
  }

  public PageInformationDto total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Total number of matched record
   * @return total
  */
  @ApiModelProperty(example = "256", value = "Total number of matched record")


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PageInformationDto pageInformation = (PageInformationDto) o;
    return Objects.equals(this.start, pageInformation.start) &&
        Objects.equals(this.end, pageInformation.end) &&
        Objects.equals(this.total, pageInformation.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(start, end, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PageInformationDto {\n");
    
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

