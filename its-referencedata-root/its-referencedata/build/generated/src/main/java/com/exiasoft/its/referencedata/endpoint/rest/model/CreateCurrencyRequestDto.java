package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestNameDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestShortNameDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CreateCurrencyRequestDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public class CreateCurrencyRequestDto   {
  @JsonProperty("currencyCode")
  private String currencyCode;

  @JsonProperty("name")
  @Valid
  private List<CreateCurrencyRequestNameDto> name = new ArrayList<>();

  @JsonProperty("shortName")
  @Valid
  private List<CreateCurrencyRequestShortNameDto> shortName = new ArrayList<>();

  @JsonProperty("decimalPlace")
  private Integer decimalPlace;

  public CreateCurrencyRequestDto currencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
    return this;
  }

  /**
   * ISO Currency Code
   * @return currencyCode
  */
  @ApiModelProperty(example = "HKD", required = true, value = "ISO Currency Code")
  @NotNull

@Size(min=3,max=3) 
  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public CreateCurrencyRequestDto name(List<CreateCurrencyRequestNameDto> name) {
    this.name = name;
    return this;
  }

  public CreateCurrencyRequestDto addNameItem(CreateCurrencyRequestNameDto nameItem) {
    this.name.add(nameItem);
    return this;
  }

  /**
   * Get name
   * @return name
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<CreateCurrencyRequestNameDto> getName() {
    return name;
  }

  public void setName(List<CreateCurrencyRequestNameDto> name) {
    this.name = name;
  }

  public CreateCurrencyRequestDto shortName(List<CreateCurrencyRequestShortNameDto> shortName) {
    this.shortName = shortName;
    return this;
  }

  public CreateCurrencyRequestDto addShortNameItem(CreateCurrencyRequestShortNameDto shortNameItem) {
    this.shortName.add(shortNameItem);
    return this;
  }

  /**
   * Get shortName
   * @return shortName
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<CreateCurrencyRequestShortNameDto> getShortName() {
    return shortName;
  }

  public void setShortName(List<CreateCurrencyRequestShortNameDto> shortName) {
    this.shortName = shortName;
  }

  public CreateCurrencyRequestDto decimalPlace(Integer decimalPlace) {
    this.decimalPlace = decimalPlace;
    return this;
  }

  /**
   * Number of decimal points supported
   * @return decimalPlace
  */
  @ApiModelProperty(required = true, value = "Number of decimal points supported")
  @NotNull


  public Integer getDecimalPlace() {
    return decimalPlace;
  }

  public void setDecimalPlace(Integer decimalPlace) {
    this.decimalPlace = decimalPlace;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateCurrencyRequestDto createCurrencyRequest = (CreateCurrencyRequestDto) o;
    return Objects.equals(this.currencyCode, createCurrencyRequest.currencyCode) &&
        Objects.equals(this.name, createCurrencyRequest.name) &&
        Objects.equals(this.shortName, createCurrencyRequest.shortName) &&
        Objects.equals(this.decimalPlace, createCurrencyRequest.decimalPlace);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currencyCode, name, shortName, decimalPlace);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateCurrencyRequestDto {\n");
    
    sb.append("    currencyCode: ").append(toIndentedString(currencyCode)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    shortName: ").append(toIndentedString(shortName)).append("\n");
    sb.append("    decimalPlace: ").append(toIndentedString(decimalPlace)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

