package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.PageInformationDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PageOfCurrencyDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public class PageOfCurrencyDto extends PageInformationDto  {
  @JsonProperty("data")
  @Valid
  private List<CurrencyDto> data = null;

  public PageOfCurrencyDto data(List<CurrencyDto> data) {
    this.data = data;
    return this;
  }

  public PageOfCurrencyDto addDataItem(CurrencyDto dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<CurrencyDto> getData() {
    return data;
  }

  public void setData(List<CurrencyDto> data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PageOfCurrencyDto pageOfCurrency = (PageOfCurrencyDto) o;
    return Objects.equals(this.data, pageOfCurrency.data) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PageOfCurrencyDto {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

