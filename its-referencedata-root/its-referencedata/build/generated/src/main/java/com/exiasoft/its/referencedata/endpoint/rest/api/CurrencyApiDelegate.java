package com.exiasoft.its.referencedata.endpoint.rest.api;

import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.ErrorResponseDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.PageOfCurrencyDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link CurrencyApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public interface CurrencyApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /v1/currencies : Create currency
     * Create a new currency
     *
     * @param createCurrencyRequestDto Currency object to create (required)
     * @return Successful operation (status code 201)
     *         or Input information is invalid (status code 400)
     *         or Unexpected error occurrs (status code 500)
     * @see CurrencyApi#createCurrency
     */
    default ResponseEntity<CurrencyDto> createCurrency(CreateCurrencyRequestDto createCurrencyRequestDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"version\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /v1/currencies : Find currency
     * Find currencies by criteria
     *
     * @param code Find user with matched userid (exact match) (optional)
     * @param name Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param shortName Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param offset Paging criteria, skip this number of record.  Default is 0 (optional, default to 0)
     * @param limit Paging criteria, skip this number of record.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;code&quot;)
     * @return Successful operation (status code 200)
     *         or Input search criteria is invalid (status code 400)
     *         or Unexpected error occurrs (status code 500)
     * @see CurrencyApi#findCurrency
     */
    default ResponseEntity<PageOfCurrencyDto> findCurrency(String code,
        String name,
        String shortName,
        Integer offset,
        Integer limit,
        String sort) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"data\" : [ { \"version\" : 0 }, { \"version\" : 0 } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /v1/currencies/{currencyCode} : Get a single currency
     * Get a single currency by currencyCode
     *
     * @param currencyCode Code of the currency to get (required)
     * @return Successful operation (status code 200)
     *         or No currency is found with input code (status code 404)
     *         or Unexpected error occurrs (status code 500)
     * @see CurrencyApi#getCurrency
     */
    default ResponseEntity<CurrencyDto> getCurrency(String currencyCode) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"version\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * PUT /v1/currencies/{currencyCode} : Update a single currency
     * Update a single currency by currency code
     *
     * @param currencyCode Code of the currency to update (required)
     * @param currencyDto Currency object to update (required)
     * @return Successful operation (status code 200)
     *         or Input information is invalid (status code 400)
     *         or No currency is found with input code (status code 404)
     *         or Unexpected error occurrs (status code 200)
     * @see CurrencyApi#putCurrency
     */
    default ResponseEntity<CurrencyDto> putCurrency(String currencyCode,
        CurrencyDto currencyDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"version\" : 0 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
