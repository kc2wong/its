package com.exiasoft.its.referencedata.endpoint.rest.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

@Controller
@RequestMapping("${openapi.iTSReferenceDataServiceOpenAPI30.base-path:/referencedata-service}")
public class CurrencyApiController implements CurrencyApi {

    private final CurrencyApiDelegate delegate;

    public CurrencyApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) CurrencyApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new CurrencyApiDelegate() {});
    }

    @Override
    public CurrencyApiDelegate getDelegate() {
        return delegate;
    }

}
