package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CreateCurrencyRequestShortNameDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public class CreateCurrencyRequestShortNameDto   {
  @JsonProperty("locale")
  private String locale;

  @JsonProperty("value")
  private String value;

  public CreateCurrencyRequestShortNameDto locale(String locale) {
    this.locale = locale;
    return this;
  }

  /**
   * BCP-47 language tag
   * @return locale
  */
  @ApiModelProperty(example = "zh-Hant", required = true, value = "BCP-47 language tag")
  @NotNull

@Size(max=10) 
  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public CreateCurrencyRequestShortNameDto value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Currency name in language specified by locale
   * @return value
  */
  @ApiModelProperty(example = "港幣", required = true, value = "Currency name in language specified by locale")
  @NotNull

@Size(max=10) 
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateCurrencyRequestShortNameDto createCurrencyRequestShortName = (CreateCurrencyRequestShortNameDto) o;
    return Objects.equals(this.locale, createCurrencyRequestShortName.locale) &&
        Objects.equals(this.value, createCurrencyRequestShortName.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(locale, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateCurrencyRequestShortNameDto {\n");
    
    sb.append("    locale: ").append(toIndentedString(locale)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

