package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestNameDto;
import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestShortNameDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CurrencyDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-29T11:55:07.655169700+08:00[Asia/Shanghai]")

public class CurrencyDto extends CreateCurrencyRequestDto  {
  @JsonProperty("version")
  private Long version;

  public CurrencyDto version(Long version) {
    this.version = version;
    return this;
  }

  /**
   * Get version
   * @return version
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CurrencyDto currency = (CurrencyDto) o;
    return Objects.equals(this.version, currency.version) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(version, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CurrencyDto {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

