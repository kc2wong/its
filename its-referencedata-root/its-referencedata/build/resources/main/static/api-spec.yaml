openapi: 3.0.2
info:
  title: ITS Reference Data Service - OpenAPI 3.0
  description: |-
    This is a sample Reference Data Service based on the OpenAPI 3.0 specification
  version: 1.0.0
servers:
  - url: http://localhost:8000/referencedata-service
tags:
  - name: currency
    description: Currency Management in ITS System

paths:
  /v1/currencies:
    post:
      tags:
        - currency
      summary: Create currency
      description: Create a new currency
      operationId: createCurrency
      security:
        - bearerAuth: []
      requestBody:
        required: true
        description: Currency object to create
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCurrencyRequest'
      responses:
        201:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Currency'
        400:
          description: Input information is invalid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        500:
          description: Unexpected error occurrs
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
    get:
      tags:
        - currency
      summary: Find currency
      description: Find currencies by criteria
      operationId: findCurrency
      security:
        - bearerAuth: []
      parameters:
        - in: query
          name: code
          schema:
            type: string
          required: false
          description: Find user with matched userid (exact match)
        - in: query
          name: name
          schema:
            type: string
          required: false
          description: Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters
        - in: query
          name: shortName
          schema:
            type: string
          required: false
          description: Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters
        - in: query
          name: offset
          schema:
            type: integer
            default: 0
          required: false
          description: Paging criteria, skip this number of record.  Default is 0
        - in: query
          name: limit
          schema:
            type: integer
            default: 25
            maximum: 200
          required: false
          description: Paging criteria, skip this number of record.  Default is 25
        - in: query
          name: sort
          schema:
            type: string
            default: code
          required: false
          description: Comma separated sorting criteria.  To sort in descending order, add "-" before the field.  E.g lastName,-email
          example: lastName,-email
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PageOfCurrency'
        400:
          description: Input search criteria is invalid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        500:
          description: Unexpected error occurrs
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'

  /v1/currencies/{currencyCode}:
    get:
      tags:
        - currency
      summary: Get a single currency
      description: Get a single currency by currencyCode
      operationId: getCurrency
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: currencyCode
          schema:
            type: string
          required: true
          description: Code of the currency to get
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Currency'
        404:
          description: No currency is found with input code
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        500:
          description: Unexpected error occurrs
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
    put:
      tags:
        - currency
      summary: Update a single currency
      description: Update a single currency by currency code
      operationId: putCurrency
      security:
        - bearerAuth: []
      parameters:
        - in: path
          name: currencyCode
          schema:
            type: string
          required: true
          description: Code of the currency to update
      requestBody:
        required: true
        description: Currency object to update
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Currency'
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Currency'
        400:
          description: Input information is invalid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        404:
          description: No currency is found with input code
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        default:
          description: Unexpected error occurrs
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'

components:
  schemas:
    CreateCurrencyRequest:
      type: object
      required:
        - currencyCode
        - name
        - shortName
        - decimalPlace
      properties:
        currencyCode:
          type: string
          description: ISO Currency Code
          example: HKD
          minLength: 3
          maxLength: 3
        name:
          type: array
          items:
            type: object
            required:
              - locale
              - value
            properties:
              locale:
                type: string
                description: BCP-47 language tag
                example: en
                maxLength: 50
              value:
                type: string
                description: Currency name in language specified by locale
                example: Hong Kong Dollar
                maxLength: 50
        shortName:
          type: array
          items:
            type: object
            required:
              - locale
              - value
            properties:
              locale:
                type: string
                description: BCP-47 language tag
                example: zh-Hant
                maxLength: 10
              value:
                type: string
                description: Currency name in language specified by locale
                example: 港幣
                maxLength: 10
        decimalPlace:
          type: integer
          format: int32
          description: Number of decimal points supported

    Currency:
      type: object
      allOf:
        - $ref: '#/components/schemas/CreateCurrencyRequest'
      required:
        - version
      properties:
        version:
          type: integer
          format: int64
    PageInformation:
      type: object
      properties:
        start:
          type: integer
          format: int32
          description: Starting record number (from 1) in matched result
          example: 26
        end:
          type: integer
          format: int32
          description: Ending record number in matched result
          example: 50
        total:
          type: integer
          format: int32
          example: 256
          description: Total number of matched record
    PageOfCurrency:
      type: object
      allOf:
        - $ref: '#/components/schemas/PageInformation'
      properties:
        data:
          type: array
          items:
            $ref: '#/components/schemas/Currency'

    ErrorResponse:
      type: object
      required:
        - errorCode
        - errorMessage
      properties:
        errorCode:
          type: string
          description:  Error code
          example: ERR-1234
        errorParam:
          type: array
          items:
            type: string
            example: D81040
        errorMessage:
          type: string
          description:  Error message in English
          example: Currency HKD already exists

  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
