CREATE TABLE `currency` (
  `record_id` char(36) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `decimal_place` smallint NOT NULL,
  `created_by` varchar(36) NOT NULL,
  `created_datetime` timestamp(6) NOT NULL,
  `updated_by` varchar(36) NOT NULL,
  `updated_datetime` timestamp(6) NOT NULL,
  `version` bigint NOT NULL,
  PRIMARY KEY (`record_id`),
  UNIQUE INDEX (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `currency_locale` (
  `record_id` char(36) NOT NULL,
  `currency_id` char(36) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `short_name` varchar(10) NOT NULL,
  `created_by` varchar(36) NOT NULL,
  `created_datetime` timestamp(6) NOT NULL,
  `updated_by` varchar(36) NOT NULL,
  `updated_datetime` timestamp(6) NOT NULL,
  `version` bigint NOT NULL,
  PRIMARY KEY (`record_id`),
  FOREIGN KEY (`currency_id`)
    REFERENCES `currency`(`record_id`)
    ON UPDATE CASCADE ON DELETE RESTRICT,
  UNIQUE INDEX (`currency_id`, `locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci