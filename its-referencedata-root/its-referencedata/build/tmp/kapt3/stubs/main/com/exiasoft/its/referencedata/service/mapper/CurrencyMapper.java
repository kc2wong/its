package com.exiasoft.its.referencedata.service.mapper;

import java.lang.System;

@org.mapstruct.Mapper()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\bg\u0018\u0000 \r2\u00020\u0001:\u0001\rJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\'J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u0003H\'J\u001a\u0010\b\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0001\u0010\u000b\u001a\u00020\tH\'J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\tH\'\u00a8\u0006\u000e"}, d2 = {"Lcom/exiasoft/its/referencedata/service/mapper/CurrencyMapper;", "", "cloneDomain", "Lcom/exiasoft/its/referencedata/service/model/Currency;", "currency", "createDomainFromRequest", "request", "Lcom/exiasoft/its/referencedata/service/model/CreateCurrencyRequest;", "domain2Entity", "Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;", "", "currencyEntity", "entity2Domain", "Companion", "its-referencedata"})
public abstract interface CurrencyMapper {
    public static final com.exiasoft.its.referencedata.service.mapper.CurrencyMapper.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "name", expression = "java(Companion.toNameDomain(currencyEntity.getLocale(), currencyEntity.getName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(currencyEntity.getLocale(), currencyEntity.getShortName()))")})
    public abstract com.exiasoft.its.referencedata.service.model.Currency entity2Domain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.entity.CurrencyEntity currencyEntity);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "id", ignore = true), @org.mapstruct.Mapping(target = "name", expression = "java(Companion.toEntityName(currency.getName(), currency.getShortName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toEntityShortName(currency.getName(), currency.getShortName()))"), @org.mapstruct.Mapping(target = "locale", expression = "java(Companion.toLocaleEntity(currency.getName(), currency.getShortName()))")})
    public abstract com.exiasoft.its.referencedata.entity.CurrencyEntity domain2Entity(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency);
    
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "id", ignore = true), @org.mapstruct.Mapping(target = "code", ignore = true), @org.mapstruct.Mapping(target = "name", expression = "java(Companion.toEntityName(currency.getName(), currency.getShortName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toEntityShortName(currency.getName(), currency.getShortName()))"), @org.mapstruct.Mapping(target = "createdBy", ignore = true), @org.mapstruct.Mapping(target = "createdDateTime", ignore = true), @org.mapstruct.Mapping(target = "locale", expression = "java(Companion.toLocaleEntity(currency.getName(), currency.getShortName()))")})
    public abstract void domain2Entity(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency, @org.jetbrains.annotations.NotNull()
    @org.mapstruct.MappingTarget()
    com.exiasoft.its.referencedata.entity.CurrencyEntity currencyEntity);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "createdBy", ignore = true), @org.mapstruct.Mapping(target = "createdDateTime", ignore = true), @org.mapstruct.Mapping(target = "updatedBy", ignore = true), @org.mapstruct.Mapping(target = "updatedDateTime", ignore = true), @org.mapstruct.Mapping(target = "version", ignore = true)})
    public abstract com.exiasoft.its.referencedata.service.model.Currency createDomainFromRequest(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest request);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {})
    public abstract com.exiasoft.its.referencedata.service.model.Currency cloneDomain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u00062\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006J.\u0010\t\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u00062\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006J4\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u00062\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006J(\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u00062\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\u000f\u001a\u00020\u0004J(\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u00062\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\u0011\u001a\u00020\u0004\u00a8\u0006\u0012"}, d2 = {"Lcom/exiasoft/its/referencedata/service/mapper/CurrencyMapper$Companion;", "", "()V", "toEntityName", "", "name", "", "Lcom/exiasoft/its/common/domain/Locale;", "shortName", "toEntityShortName", "toLocaleEntity", "", "Lcom/exiasoft/its/referencedata/entity/CurrencyLocaleEntity;", "toNameDomain", "locale", "enName", "toShortNameDomain", "enShortName", "its-referencedata"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> toLocaleEntity(@org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name, @org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String toEntityName(@org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name, @org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String toEntityShortName(@org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name, @org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> toNameDomain(@org.jetbrains.annotations.NotNull()
        java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> locale, @org.jetbrains.annotations.NotNull()
        java.lang.String enName) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> toShortNameDomain(@org.jetbrains.annotations.NotNull()
        java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> locale, @org.jetbrains.annotations.NotNull()
        java.lang.String enShortName) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}