package com.exiasoft.its.referencedata.entity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR$\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0006\"\u0004\b\u0018\u0010\bR\u001e\u0010\u0019\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0006\"\u0004\b\u001b\u0010\b\u00a8\u0006\u001c"}, d2 = {"Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;", "Lcom/exiasoft/its/common/entity/BaseEntity;", "()V", "code", "", "getCode", "()Ljava/lang/String;", "setCode", "(Ljava/lang/String;)V", "decimalPlace", "", "getDecimalPlace", "()I", "setDecimalPlace", "(I)V", "locale", "", "Lcom/exiasoft/its/referencedata/entity/CurrencyLocaleEntity;", "getLocale", "()Ljava/util/List;", "setLocale", "(Ljava/util/List;)V", "name", "getName", "setName", "shortName", "getShortName", "setShortName", "its-referencedata"})
@javax.persistence.Table(name = "CURRENCY")
@javax.persistence.NamedEntityGraph(name = "currency-locale-graph", attributeNodes = {@javax.persistence.NamedAttributeNode(value = "locale")})
@javax.persistence.Entity()
public final class CurrencyEntity extends com.exiasoft.its.common.entity.BaseEntity {
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "CODE")
    public java.lang.String code;
    @javax.persistence.Column(name = "DECIMAL_PLACE")
    private int decimalPlace = 0;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "NAME")
    public java.lang.String name;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "SHORT_NAME")
    public java.lang.String shortName;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = javax.persistence.FetchType.LAZY, mappedBy = "currency")
    public java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> locale;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCode() {
        return null;
    }
    
    public final void setCode(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getDecimalPlace() {
        return 0;
    }
    
    public final void setDecimalPlace(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getShortName() {
        return null;
    }
    
    public final void setShortName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> getLocale() {
        return null;
    }
    
    public final void setLocale(@org.jetbrains.annotations.NotNull()
    java.util.List<com.exiasoft.its.referencedata.entity.CurrencyLocaleEntity> p0) {
    }
    
    public CurrencyEntity() {
        super();
    }
}