package com.exiasoft.its.referencedata.service.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\t\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R&\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u00040\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR&\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u00040\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u001c\"\u0004\b!\u0010\u001eR\u001a\u0010\"\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0006\"\u0004\b$\u0010\bR\u001a\u0010%\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u000f\"\u0004\b\'\u0010\u0011R\u001a\u0010(\u001a\u00020)X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-\u00a8\u0006."}, d2 = {"Lcom/exiasoft/its/referencedata/service/model/Currency;", "", "()V", "code", "", "getCode", "()Ljava/lang/String;", "setCode", "(Ljava/lang/String;)V", "createdBy", "getCreatedBy", "setCreatedBy", "createdDateTime", "Ljava/time/Instant;", "getCreatedDateTime", "()Ljava/time/Instant;", "setCreatedDateTime", "(Ljava/time/Instant;)V", "decimalPlace", "", "getDecimalPlace", "()I", "setDecimalPlace", "(I)V", "name", "", "Lcom/exiasoft/its/common/domain/Locale;", "getName", "()Ljava/util/Map;", "setName", "(Ljava/util/Map;)V", "shortName", "getShortName", "setShortName", "updatedBy", "getUpdatedBy", "setUpdatedBy", "updatedDateTime", "getUpdatedDateTime", "setUpdatedDateTime", "version", "", "getVersion", "()J", "setVersion", "(J)V", "its-referencedata"})
public final class Currency {
    @org.jetbrains.annotations.NotNull()
    public java.lang.String code;
    private int decimalPlace = 0;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String createdBy;
    @org.jetbrains.annotations.NotNull()
    public java.time.Instant createdDateTime;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String updatedBy;
    @org.jetbrains.annotations.NotNull()
    public java.time.Instant updatedDateTime;
    private long version = 0L;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCode() {
        return null;
    }
    
    public final void setCode(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getDecimalPlace() {
        return 0;
    }
    
    public final void setDecimalPlace(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> getShortName() {
        return null;
    }
    
    public final void setShortName(@org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCreatedBy() {
        return null;
    }
    
    public final void setCreatedBy(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.Instant getCreatedDateTime() {
        return null;
    }
    
    public final void setCreatedDateTime(@org.jetbrains.annotations.NotNull()
    java.time.Instant p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUpdatedBy() {
        return null;
    }
    
    public final void setUpdatedBy(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.Instant getUpdatedDateTime() {
        return null;
    }
    
    public final void setUpdatedDateTime(@org.jetbrains.annotations.NotNull()
    java.time.Instant p0) {
    }
    
    public final long getVersion() {
        return 0L;
    }
    
    public final void setVersion(long p0) {
    }
    
    public Currency() {
        super();
    }
}