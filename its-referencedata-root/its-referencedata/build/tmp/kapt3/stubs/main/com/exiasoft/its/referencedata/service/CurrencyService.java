package com.exiasoft.its.referencedata.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&JM\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\r\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u000bH&\u00a2\u0006\u0002\u0010\u0012J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00032\u0006\u0010\n\u001a\u00020\u000bH&J \u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0017"}, d2 = {"Lcom/exiasoft/its/referencedata/service/CurrencyService;", "", "createCurrency", "Lcom/exiasoft/its/referencedata/service/model/Currency;", "createCurrencyRequest", "Lcom/exiasoft/its/referencedata/service/model/CreateCurrencyRequest;", "invoker", "Lcom/exiasoft/its/common/domain/UserProfile;", "findCurrency", "Lorg/springframework/data/domain/Page;", "code", "", "name", "shortName", "offset", "", "limit", "sort", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;)Lorg/springframework/data/domain/Page;", "getCurrency", "updateCurrency", "currencyCode", "currency", "its-referencedata"})
public abstract interface CurrencyService {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.exiasoft.its.referencedata.service.model.Currency createCurrency(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest createCurrencyRequest, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invoker);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.exiasoft.its.referencedata.service.model.Currency updateCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String currencyCode, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invoker);
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.exiasoft.its.referencedata.service.model.Currency getCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.springframework.data.domain.Page<com.exiasoft.its.referencedata.service.model.Currency> findCurrency(@org.jetbrains.annotations.Nullable()
    java.lang.String code, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String shortName, @org.jetbrains.annotations.Nullable()
    java.lang.Integer offset, int limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort);
}