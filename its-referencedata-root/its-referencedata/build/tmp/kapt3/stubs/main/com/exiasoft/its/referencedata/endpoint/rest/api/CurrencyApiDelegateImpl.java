package com.exiasoft.its.referencedata.endpoint.rest.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0017\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016JO\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00172\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001e2\b\u0010 \u001a\u0004\u0018\u00010\u001e2\b\u0010!\u001a\u0004\u0018\u00010\u00032\b\u0010\"\u001a\u0004\u0018\u00010\u00032\b\u0010#\u001a\u0004\u0018\u00010\u001eH\u0016\u00a2\u0006\u0002\u0010$J\u0016\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u001e\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\'\u001a\u00020\u0018H\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u00020\u00118\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006("}, d2 = {"Lcom/exiasoft/its/referencedata/endpoint/rest/api/CurrencyApiDelegateImpl;", "Lcom/exiasoft/its/referencedata/endpoint/rest/api/CurrencyApiDelegate;", "defaultPageSize", "", "currencyService", "Lcom/exiasoft/its/referencedata/service/CurrencyService;", "(ILcom/exiasoft/its/referencedata/service/CurrencyService;)V", "getCurrencyService", "()Lcom/exiasoft/its/referencedata/service/CurrencyService;", "getDefaultPageSize", "()I", "logger", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "mapper", "Lcom/exiasoft/its/referencedata/endpoint/rest/mapper/CurrencyDtoMapper;", "requestData", "Lcom/exiasoft/its/common/bean/RequestData;", "getRequestData", "()Lcom/exiasoft/its/common/bean/RequestData;", "setRequestData", "(Lcom/exiasoft/its/common/bean/RequestData;)V", "createCurrency", "Lorg/springframework/http/ResponseEntity;", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CurrencyDto;", "createCurrencyRequestDto", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CreateCurrencyRequestDto;", "findCurrency", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/PageOfCurrencyDto;", "currencyCode", "", "name", "shortName", "offset", "limit", "sort", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)Lorg/springframework/http/ResponseEntity;", "getCurrency", "putCurrency", "currencyDto", "its-referencedata"})
@org.springframework.stereotype.Component()
public class CurrencyApiDelegateImpl implements com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApiDelegate {
    private final org.slf4j.Logger logger = null;
    private final com.exiasoft.its.referencedata.endpoint.rest.mapper.CurrencyDtoMapper mapper = null;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.exiasoft.its.common.bean.RequestData requestData;
    private final int defaultPageSize = 0;
    @org.jetbrains.annotations.NotNull()
    private final com.exiasoft.its.referencedata.service.CurrencyService currencyService = null;
    
    @org.jetbrains.annotations.NotNull()
    public com.exiasoft.its.common.bean.RequestData getRequestData() {
        return null;
    }
    
    public void setRequestData(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.bean.RequestData p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto> createCurrency(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto createCurrencyRequestDto) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.referencedata.endpoint.rest.model.PageOfCurrencyDto> findCurrency(@org.jetbrains.annotations.Nullable()
    java.lang.String currencyCode, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String shortName, @org.jetbrains.annotations.Nullable()
    java.lang.Integer offset, @org.jetbrains.annotations.Nullable()
    java.lang.Integer limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto> getCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String currencyCode) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto> putCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String currencyCode, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto currencyDto) {
        return null;
    }
    
    public int getDefaultPageSize() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.exiasoft.its.referencedata.service.CurrencyService getCurrencyService() {
        return null;
    }
    
    public CurrencyApiDelegateImpl(@org.springframework.beans.factory.annotation.Value(value = "${application.defaultPageSize}")
    int defaultPageSize, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.exiasoft.its.referencedata.service.CurrencyService currencyService) {
        super();
    }
}