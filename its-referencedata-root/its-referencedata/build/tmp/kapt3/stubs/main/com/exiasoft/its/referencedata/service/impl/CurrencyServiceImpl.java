package com.exiasoft.its.referencedata.service.impl;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\b\u0017\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0017JT\u0010\u0012\u001a&\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u00140\u00132\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u00142\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u0014H\u0012JM\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\r0\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u00162\b\u0010\u001c\u001a\u0004\u0018\u00010\u00162\b\u0010\u001d\u001a\u0004\u0018\u00010\u00162\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020\u001f2\b\u0010!\u001a\u0004\u0018\u00010\u0016H\u0016\u00a2\u0006\u0002\u0010\"J\u0012\u0010#\u001a\u0004\u0018\u00010\r2\u0006\u0010\u001b\u001a\u00020\u0016H\u0016J \u0010$\u001a\u00020\r2\u0006\u0010%\u001a\u00020\u00162\u0006\u0010&\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0011H\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/exiasoft/its/referencedata/service/impl/CurrencyServiceImpl;", "Lcom/exiasoft/its/referencedata/service/CurrencyService;", "currencyRepository", "Lcom/exiasoft/its/referencedata/entity/repo/CurrencyRepository;", "(Lcom/exiasoft/its/referencedata/entity/repo/CurrencyRepository;)V", "getCurrencyRepository", "()Lcom/exiasoft/its/referencedata/entity/repo/CurrencyRepository;", "logger", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "mapper", "Lcom/exiasoft/its/referencedata/service/mapper/CurrencyMapper;", "createCurrency", "Lcom/exiasoft/its/referencedata/service/model/Currency;", "createCurrencyRequest", "Lcom/exiasoft/its/referencedata/service/model/CreateCurrencyRequest;", "invoker", "Lcom/exiasoft/its/common/domain/UserProfile;", "enrichName", "Lkotlin/Pair;", "", "Lcom/exiasoft/its/common/domain/Locale;", "", "nameMap", "shortNameMap", "findCurrency", "Lorg/springframework/data/domain/Page;", "code", "name", "shortName", "offset", "", "limit", "sort", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;)Lorg/springframework/data/domain/Page;", "getCurrency", "updateCurrency", "currencyCode", "currency", "its-referencedata"})
@org.springframework.stereotype.Service()
public class CurrencyServiceImpl implements com.exiasoft.its.referencedata.service.CurrencyService {
    private final com.exiasoft.its.referencedata.service.mapper.CurrencyMapper mapper = null;
    private final org.slf4j.Logger logger = null;
    @org.jetbrains.annotations.NotNull()
    private final com.exiasoft.its.referencedata.entity.repo.CurrencyRepository currencyRepository = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    @javax.transaction.Transactional()
    public com.exiasoft.its.referencedata.service.model.Currency createCurrency(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest createCurrencyRequest, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invoker) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    @javax.transaction.Transactional()
    public com.exiasoft.its.referencedata.service.model.Currency updateCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String currencyCode, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invoker) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.exiasoft.its.referencedata.service.model.Currency getCurrency(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.data.domain.Page<com.exiasoft.its.referencedata.service.model.Currency> findCurrency(@org.jetbrains.annotations.Nullable()
    java.lang.String code, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String shortName, @org.jetbrains.annotations.Nullable()
    java.lang.Integer offset, int limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort) {
        return null;
    }
    
    private kotlin.Pair<java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String>, java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String>> enrichName(java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> nameMap, java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortNameMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.exiasoft.its.referencedata.entity.repo.CurrencyRepository getCurrencyRepository() {
        return null;
    }
    
    public CurrencyServiceImpl(@org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.exiasoft.its.referencedata.entity.repo.CurrencyRepository currencyRepository) {
        super();
    }
}