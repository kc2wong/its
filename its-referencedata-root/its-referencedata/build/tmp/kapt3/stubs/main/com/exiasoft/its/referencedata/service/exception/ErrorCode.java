package com.exiasoft.its.referencedata.service.exception;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/exiasoft/its/referencedata/service/exception/ErrorCode;", "Lcom/exiasoft/its/common/exception/ErrorMessageProvider;", "()V", "Companion", "its-referencedata"})
public final class ErrorCode implements com.exiasoft.its.common.exception.ErrorMessageProvider {
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Currency with code %s already exists")
    public static final java.lang.String CURRENCY_ALREADY_EXISTS = "REFDATA.00001";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Currency with code %s is not found")
    public static final java.lang.String CURRENCY_NOT_FOUND = "REFDATA.00002";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Sort criteria %s is invalid")
    public static final java.lang.String INVALID_SORT_CRITERIA = "REFDATA.00003";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Missing %s name or shortname")
    public static final java.lang.String MISSING_ENGLISH_NAME = "REFDATA.00003";
    public static final com.exiasoft.its.referencedata.service.exception.ErrorCode.Companion Companion = null;
    
    public ErrorCode() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/exiasoft/its/referencedata/service/exception/ErrorCode$Companion;", "", "()V", "CURRENCY_ALREADY_EXISTS", "", "CURRENCY_NOT_FOUND", "INVALID_SORT_CRITERIA", "MISSING_ENGLISH_NAME", "its-referencedata"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}