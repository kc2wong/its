package com.exiasoft.its.referencedata.service.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\u0015\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003JI\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u00072\u0014\b\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR&\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R&\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00030\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0010\"\u0004\b\u0014\u0010\u0012\u00a8\u0006\u001f"}, d2 = {"Lcom/exiasoft/its/referencedata/service/model/CreateCurrencyRequest;", "", "code", "", "decimalPlace", "", "shortName", "", "Lcom/exiasoft/its/common/domain/Locale;", "name", "(Ljava/lang/String;ILjava/util/Map;Ljava/util/Map;)V", "getCode", "()Ljava/lang/String;", "getDecimalPlace", "()I", "getName", "()Ljava/util/Map;", "setName", "(Ljava/util/Map;)V", "getShortName", "setShortName", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "toString", "its-referencedata"})
public final class CreateCurrencyRequest {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String code = null;
    private final int decimalPlace = 0;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCode() {
        return null;
    }
    
    public final int getDecimalPlace() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> getShortName() {
        return null;
    }
    
    public final void setShortName(@org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> p0) {
    }
    
    public CreateCurrencyRequest(@org.jetbrains.annotations.NotNull()
    java.lang.String code, int decimalPlace, @org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName, @org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    public final int component2() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest copy(@org.jetbrains.annotations.NotNull()
    java.lang.String code, int decimalPlace, @org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> shortName, @org.jetbrains.annotations.NotNull()
    java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}