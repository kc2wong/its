package com.exiasoft.its.referencedata.endpoint.rest.mapper;

import java.lang.System;

@org.mapstruct.Mapper(uses = {com.exiasoft.its.common.endpoint.rest.mapper.BaseDtoMapper.class})
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u0000 \n2\u00020\u0001:\u0001\nJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\'J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0003H\'\u00a8\u0006\u000b"}, d2 = {"Lcom/exiasoft/its/referencedata/endpoint/rest/mapper/CurrencyDtoMapper;", "", "domain2Dto", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CurrencyDto;", "currency", "Lcom/exiasoft/its/referencedata/service/model/Currency;", "dto2Domain", "Lcom/exiasoft/its/referencedata/service/model/CreateCurrencyRequest;", "request", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CreateCurrencyRequestDto;", "Companion", "its-referencedata"})
public abstract interface CurrencyDtoMapper {
    public static final com.exiasoft.its.referencedata.endpoint.rest.mapper.CurrencyDtoMapper.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "currencyCode", source = "code"), @org.mapstruct.Mapping(target = "name", expression = "java(Companion.toNameDto(currency.getName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toShortNameDto(currency.getShortName()))")})
    public abstract com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto domain2Dto(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.service.model.Currency currency);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "code", source = "currencyCode"), @org.mapstruct.Mapping(target = "name", expression = "java(Companion.toNameDomain(request.getName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(request.getShortName()))")})
    public abstract com.exiasoft.its.referencedata.service.model.CreateCurrencyRequest dto2Domain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestDto request);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "code", source = "currencyCode"), @org.mapstruct.Mapping(target = "name", expression = "java(Companion.toNameDomain(request.getName()))"), @org.mapstruct.Mapping(target = "shortName", expression = "java(Companion.toShortNameDomain(request.getShortName()))"), @org.mapstruct.Mapping(target = "createdBy", ignore = true), @org.mapstruct.Mapping(target = "createdDateTime", ignore = true), @org.mapstruct.Mapping(target = "updatedBy", ignore = true), @org.mapstruct.Mapping(target = "updatedDateTime", ignore = true)})
    public abstract com.exiasoft.its.referencedata.service.model.Currency dto2Domain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.referencedata.endpoint.rest.model.CurrencyDto request);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bJ \u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004J \u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\f0\bJ \u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\b2\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a8\u0006\u000e"}, d2 = {"Lcom/exiasoft/its/referencedata/endpoint/rest/mapper/CurrencyDtoMapper$Companion;", "", "()V", "toNameDomain", "", "Lcom/exiasoft/its/common/domain/Locale;", "", "name", "", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CreateCurrencyRequestNameDto;", "toNameDto", "toShortNameDomain", "Lcom/exiasoft/its/referencedata/endpoint/rest/model/CreateCurrencyRequestShortNameDto;", "toShortNameDto", "its-referencedata"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestNameDto> toNameDto(@org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestShortNameDto> toShortNameDto(@org.jetbrains.annotations.NotNull()
        java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> name) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> toNameDomain(@org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestNameDto> name) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<com.exiasoft.its.common.domain.Locale, java.lang.String> toShortNameDomain(@org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequestShortNameDto> name) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}