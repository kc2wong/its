package com.exiasoft.its.referencedata.entity.repo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\bg\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\b\u0012\u0004\u0012\u00020\u00020\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0006\u001a\u00020\u0003H\'J\u001c\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\bH\'\u00a8\u0006\n"}, d2 = {"Lcom/exiasoft/its/referencedata/entity/repo/CurrencyRepository;", "Lorg/springframework/data/jpa/repository/JpaRepository;", "Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;", "", "Lorg/springframework/data/jpa/repository/JpaSpecificationExecutor;", "findByCode", "code", "findByIdIn", "", "id", "its-referencedata"})
@org.springframework.stereotype.Repository()
public abstract interface CurrencyRepository extends org.springframework.data.jpa.repository.JpaRepository<com.exiasoft.its.referencedata.entity.CurrencyEntity, java.lang.String>, org.springframework.data.jpa.repository.JpaSpecificationExecutor<com.exiasoft.its.referencedata.entity.CurrencyEntity> {
    
    @org.jetbrains.annotations.Nullable()
    @org.springframework.data.jpa.repository.EntityGraph(value = "currency-locale-graph", type = org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.LOAD)
    public abstract com.exiasoft.its.referencedata.entity.CurrencyEntity findByCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.data.jpa.repository.EntityGraph(value = "currency-locale-graph", type = org.springframework.data.jpa.repository.EntityGraph.EntityGraphType.LOAD)
    public abstract java.util.List<com.exiasoft.its.referencedata.entity.CurrencyEntity> findByIdIn(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> id);
}