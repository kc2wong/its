package com.exiasoft.its.referencedata.entity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0015\u001a\u00020\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014\u00a8\u0006\u0018"}, d2 = {"Lcom/exiasoft/its/referencedata/entity/CurrencyLocaleEntity;", "Lcom/exiasoft/its/common/entity/BaseEntity;", "()V", "currency", "Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;", "getCurrency", "()Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;", "setCurrency", "(Lcom/exiasoft/its/referencedata/entity/CurrencyEntity;)V", "locale", "Lcom/exiasoft/its/common/domain/Locale;", "getLocale", "()Lcom/exiasoft/its/common/domain/Locale;", "setLocale", "(Lcom/exiasoft/its/common/domain/Locale;)V", "name", "", "getName", "()Ljava/lang/String;", "setName", "(Ljava/lang/String;)V", "shortName", "getShortName", "setShortName", "its-referencedata"})
@javax.persistence.Table(name = "CURRENCY_LOCALE")
@javax.persistence.Entity()
public final class CurrencyLocaleEntity extends com.exiasoft.its.common.entity.BaseEntity {
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Convert(converter = com.exiasoft.its.common.entity.converter.LocaleConverter.class)
    @javax.persistence.Column(name = "LOCALE")
    public com.exiasoft.its.common.domain.Locale locale;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "SHORT_NAME")
    public java.lang.String shortName;
    @org.jetbrains.annotations.NotNull()
    @javax.persistence.Column(name = "NAME")
    public java.lang.String name;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.JoinColumn(name = "currency_id", nullable = false)
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    private com.exiasoft.its.referencedata.entity.CurrencyEntity currency;
    
    @org.jetbrains.annotations.NotNull()
    public final com.exiasoft.its.common.domain.Locale getLocale() {
        return null;
    }
    
    public final void setLocale(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.Locale p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getShortName() {
        return null;
    }
    
    public final void setShortName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.exiasoft.its.referencedata.entity.CurrencyEntity getCurrency() {
        return null;
    }
    
    public final void setCurrency(@org.jetbrains.annotations.Nullable()
    com.exiasoft.its.referencedata.entity.CurrencyEntity p0) {
    }
    
    public CurrencyLocaleEntity() {
        super();
    }
}