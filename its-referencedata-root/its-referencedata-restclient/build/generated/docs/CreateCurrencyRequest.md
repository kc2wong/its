

# CreateCurrencyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currencyCode** | **String** | ISO Currency Code | 
**name** | [**List&lt;CreateCurrencyRequestName&gt;**](CreateCurrencyRequestName.md) |  | 
**shortName** | [**List&lt;CreateCurrencyRequestShortName&gt;**](CreateCurrencyRequestShortName.md) |  | 
**decimalPlace** | **Integer** | Number of decimal points supported | 



