

# CreateCurrencyRequestName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **String** | BCP-47 language tag | 
**value** | **String** | Currency name in language specified by locale | 



