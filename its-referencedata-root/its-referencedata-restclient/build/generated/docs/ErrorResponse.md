

# ErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **String** | Error code | 
**errorParam** | **List&lt;String&gt;** |  |  [optional]
**errorMessage** | **String** | Error message in English | 



