# CurrencyApi

All URIs are relative to *http://localhost:8000/referencedata-service*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCurrency**](CurrencyApi.md#createCurrency) | **POST** /v1/currencies | Create currency
[**findCurrency**](CurrencyApi.md#findCurrency) | **GET** /v1/currencies | Find currency
[**getCurrency**](CurrencyApi.md#getCurrency) | **GET** /v1/currencies/{currencyCode} | Get a single currency
[**putCurrency**](CurrencyApi.md#putCurrency) | **PUT** /v1/currencies/{currencyCode} | Update a single currency



## createCurrency

> Currency createCurrency(createCurrencyRequest)

Create currency

Create a new currency

### Example

```java
// Import classes:
import com.exiasoft.its.referencedata.ApiClient;
import com.exiasoft.its.referencedata.ApiException;
import com.exiasoft.its.referencedata.Configuration;
import com.exiasoft.its.referencedata.auth.*;
import com.exiasoft.its.referencedata.models.*;
import com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/referencedata-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        CurrencyApi apiInstance = new CurrencyApi(defaultClient);
        CreateCurrencyRequest createCurrencyRequest = new CreateCurrencyRequest(); // CreateCurrencyRequest | Currency object to create
        try {
            Currency result = apiInstance.createCurrency(createCurrencyRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CurrencyApi#createCurrency");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createCurrencyRequest** | [**CreateCurrencyRequest**](CreateCurrencyRequest.md)| Currency object to create |

### Return type

[**Currency**](Currency.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Successful operation |  -  |
| **400** | Input information is invalid |  -  |
| **500** | Unexpected error occurrs |  -  |


## findCurrency

> PageOfCurrency findCurrency(code, name, shortName, offset, limit, sort)

Find currency

Find currencies by criteria

### Example

```java
// Import classes:
import com.exiasoft.its.referencedata.ApiClient;
import com.exiasoft.its.referencedata.ApiException;
import com.exiasoft.its.referencedata.Configuration;
import com.exiasoft.its.referencedata.auth.*;
import com.exiasoft.its.referencedata.models.*;
import com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/referencedata-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        CurrencyApi apiInstance = new CurrencyApi(defaultClient);
        String code = "code_example"; // String | Find user with matched userid (exact match)
        String name = "name_example"; // String | Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters
        String shortName = "shortName_example"; // String | Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters
        Integer offset = 0; // Integer | Paging criteria, skip this number of record.  Default is 0
        Integer limit = 25; // Integer | Paging criteria, skip this number of record.  Default is 25
        String sort = lastName,-email; // String | Comma separated sorting criteria.  To sort in descending order, add \"-\" before the field.  E.g lastName,-email
        try {
            PageOfCurrency result = apiInstance.findCurrency(code, name, shortName, offset, limit, sort);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CurrencyApi#findCurrency");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**| Find user with matched userid (exact match) | [optional]
 **name** | **String**| Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters | [optional]
 **shortName** | **String**| Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters | [optional]
 **offset** | **Integer**| Paging criteria, skip this number of record.  Default is 0 | [optional] [default to 0]
 **limit** | **Integer**| Paging criteria, skip this number of record.  Default is 25 | [optional] [default to 25]
 **sort** | **String**| Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email | [optional] [default to &quot;code&quot;]

### Return type

[**PageOfCurrency**](PageOfCurrency.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Input search criteria is invalid |  -  |
| **500** | Unexpected error occurrs |  -  |


## getCurrency

> Currency getCurrency(currencyCode)

Get a single currency

Get a single currency by currencyCode

### Example

```java
// Import classes:
import com.exiasoft.its.referencedata.ApiClient;
import com.exiasoft.its.referencedata.ApiException;
import com.exiasoft.its.referencedata.Configuration;
import com.exiasoft.its.referencedata.auth.*;
import com.exiasoft.its.referencedata.models.*;
import com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/referencedata-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        CurrencyApi apiInstance = new CurrencyApi(defaultClient);
        String currencyCode = "currencyCode_example"; // String | Code of the currency to get
        try {
            Currency result = apiInstance.getCurrency(currencyCode);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CurrencyApi#getCurrency");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **currencyCode** | **String**| Code of the currency to get |

### Return type

[**Currency**](Currency.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **404** | No currency is found with input code |  -  |
| **500** | Unexpected error occurrs |  -  |


## putCurrency

> Currency putCurrency(currencyCode, currency)

Update a single currency

Update a single currency by currency code

### Example

```java
// Import classes:
import com.exiasoft.its.referencedata.ApiClient;
import com.exiasoft.its.referencedata.ApiException;
import com.exiasoft.its.referencedata.Configuration;
import com.exiasoft.its.referencedata.auth.*;
import com.exiasoft.its.referencedata.models.*;
import com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/referencedata-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        CurrencyApi apiInstance = new CurrencyApi(defaultClient);
        String currencyCode = "currencyCode_example"; // String | Code of the currency to update
        Currency currency = new Currency(); // Currency | Currency object to update
        try {
            Currency result = apiInstance.putCurrency(currencyCode, currency);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CurrencyApi#putCurrency");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **currencyCode** | **String**| Code of the currency to update |
 **currency** | [**Currency**](Currency.md)| Currency object to update |

### Return type

[**Currency**](Currency.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Input information is invalid |  -  |
| **404** | No currency is found with input code |  -  |
| **0** | Unexpected error occurrs |  -  |

