package com.exiasoft.its.referencedata.endpoint.rest.api;

import com.exiasoft.its.referencedata.ApiClient;

import com.exiasoft.its.referencedata.endpoint.rest.model.CreateCurrencyRequest;
import com.exiasoft.its.referencedata.endpoint.rest.model.Currency;
import com.exiasoft.its.referencedata.endpoint.rest.model.ErrorResponse;
import com.exiasoft.its.referencedata.endpoint.rest.model.PageOfCurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-11-09T23:42:41.709312700+08:00[Asia/Shanghai]")
@Component("com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi")
public class CurrencyApi {
    private ApiClient apiClient;

    public CurrencyApi() {
        this(new ApiClient());
    }

    @Autowired
    public CurrencyApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Create currency
     * Create a new currency
     * <p><b>201</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>500</b> - Unexpected error occurrs
     * @param createCurrencyRequest Currency object to create (required)
     * @return Currency
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Currency createCurrency(CreateCurrencyRequest createCurrencyRequest) throws RestClientException {
        return createCurrencyWithHttpInfo(createCurrencyRequest).getBody();
    }

    /**
     * Create currency
     * Create a new currency
     * <p><b>201</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>500</b> - Unexpected error occurrs
     * @param createCurrencyRequest Currency object to create (required)
     * @return ResponseEntity&lt;Currency&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Currency> createCurrencyWithHttpInfo(CreateCurrencyRequest createCurrencyRequest) throws RestClientException {
        Object postBody = createCurrencyRequest;
        
        // verify the required parameter 'createCurrencyRequest' is set
        if (createCurrencyRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'createCurrencyRequest' when calling createCurrency");
        }
        
        String path = apiClient.expandPath("/v1/currencies", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<Currency> returnType = new ParameterizedTypeReference<Currency>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Find currency
     * Find currencies by criteria
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input search criteria is invalid
     * <p><b>500</b> - Unexpected error occurrs
     * @param code Find user with matched userid (exact match) (optional)
     * @param name Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param shortName Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param offset Paging criteria, skip this number of record.  Default is 0 (optional, default to 0)
     * @param limit Paging criteria, skip this number of record.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;code&quot;)
     * @return PageOfCurrency
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public PageOfCurrency findCurrency(String code, String name, String shortName, Integer offset, Integer limit, String sort) throws RestClientException {
        return findCurrencyWithHttpInfo(code, name, shortName, offset, limit, sort).getBody();
    }

    /**
     * Find currency
     * Find currencies by criteria
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input search criteria is invalid
     * <p><b>500</b> - Unexpected error occurrs
     * @param code Find user with matched userid (exact match) (optional)
     * @param name Find currency with matched name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param shortName Find currency with matched short name (case insenitive, partial match).  Must input at least 3 characters (optional)
     * @param offset Paging criteria, skip this number of record.  Default is 0 (optional, default to 0)
     * @param limit Paging criteria, skip this number of record.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;code&quot;)
     * @return ResponseEntity&lt;PageOfCurrency&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<PageOfCurrency> findCurrencyWithHttpInfo(String code, String name, String shortName, Integer offset, Integer limit, String sort) throws RestClientException {
        Object postBody = null;
        
        String path = apiClient.expandPath("/v1/currencies", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "code", code));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "name", name));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "shortName", shortName));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "offset", offset));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "sort", sort));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<PageOfCurrency> returnType = new ParameterizedTypeReference<PageOfCurrency>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get a single currency
     * Get a single currency by currencyCode
     * <p><b>200</b> - Successful operation
     * <p><b>404</b> - No currency is found with input code
     * <p><b>500</b> - Unexpected error occurrs
     * @param currencyCode Code of the currency to get (required)
     * @return Currency
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Currency getCurrency(String currencyCode) throws RestClientException {
        return getCurrencyWithHttpInfo(currencyCode).getBody();
    }

    /**
     * Get a single currency
     * Get a single currency by currencyCode
     * <p><b>200</b> - Successful operation
     * <p><b>404</b> - No currency is found with input code
     * <p><b>500</b> - Unexpected error occurrs
     * @param currencyCode Code of the currency to get (required)
     * @return ResponseEntity&lt;Currency&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Currency> getCurrencyWithHttpInfo(String currencyCode) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'currencyCode' is set
        if (currencyCode == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'currencyCode' when calling getCurrency");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("currencyCode", currencyCode);
        String path = apiClient.expandPath("/v1/currencies/{currencyCode}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<Currency> returnType = new ParameterizedTypeReference<Currency>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Update a single currency
     * Update a single currency by currency code
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>404</b> - No currency is found with input code
     * <p><b>0</b> - Unexpected error occurrs
     * @param currencyCode Code of the currency to update (required)
     * @param currency Currency object to update (required)
     * @return Currency
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Currency putCurrency(String currencyCode, Currency currency) throws RestClientException {
        return putCurrencyWithHttpInfo(currencyCode, currency).getBody();
    }

    /**
     * Update a single currency
     * Update a single currency by currency code
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>404</b> - No currency is found with input code
     * <p><b>0</b> - Unexpected error occurrs
     * @param currencyCode Code of the currency to update (required)
     * @param currency Currency object to update (required)
     * @return ResponseEntity&lt;Currency&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Currency> putCurrencyWithHttpInfo(String currencyCode, Currency currency) throws RestClientException {
        Object postBody = currency;
        
        // verify the required parameter 'currencyCode' is set
        if (currencyCode == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'currencyCode' when calling putCurrency");
        }
        
        // verify the required parameter 'currency' is set
        if (currency == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'currency' when calling putCurrency");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("currencyCode", currencyCode);
        String path = apiClient.expandPath("/v1/currencies/{currencyCode}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<Currency> returnType = new ParameterizedTypeReference<Currency>() {};
        return apiClient.invokeAPI(path, HttpMethod.PUT, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
}
