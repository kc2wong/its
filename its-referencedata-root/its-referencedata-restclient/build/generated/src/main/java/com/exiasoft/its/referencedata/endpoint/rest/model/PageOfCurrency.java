/*
 * ITS Reference Data Service - OpenAPI 3.0
 * This is a sample Reference Data Service based on the OpenAPI 3.0 specification
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.exiasoft.its.referencedata.endpoint.rest.model;

import java.util.Objects;
import java.util.Arrays;
import com.exiasoft.its.referencedata.endpoint.rest.model.Currency;
import com.exiasoft.its.referencedata.endpoint.rest.model.PageInformation;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * PageOfCurrency
 */
@JsonPropertyOrder({
  PageOfCurrency.JSON_PROPERTY_DATA
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-11-09T23:42:41.709312700+08:00[Asia/Shanghai]")
public class PageOfCurrency extends PageInformation {
  public static final String JSON_PROPERTY_DATA = "data";
  private List<Currency> data = null;


  public PageOfCurrency data(List<Currency> data) {
    
    this.data = data;
    return this;
  }

  public PageOfCurrency addDataItem(Currency dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }
    this.data.add(dataItem);
    return this;
  }

   /**
   * Get data
   * @return data
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")
  @JsonProperty(JSON_PROPERTY_DATA)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public List<Currency> getData() {
    return data;
  }


  public void setData(List<Currency> data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PageOfCurrency pageOfCurrency = (PageOfCurrency) o;
    return Objects.equals(this.data, pageOfCurrency.data) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PageOfCurrency {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

