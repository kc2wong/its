# openapi-java-client

ITS Reference Data Service - OpenAPI 3.0

- API version: 1.0.0

- Build date: 2020-11-08T23:42:06.010076500+08:00[Asia/Shanghai]

This is a sample Reference Data Service based on the OpenAPI 3.0 specification


*Automatically generated by the [OpenAPI Generator](https://openapi-generator.tech)*

## Requirements

Building the API client library requires:

1. Java 1.8+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>org.openapitools</groupId>
  <artifactId>openapi-java-client</artifactId>
  <version>1.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "org.openapitools:openapi-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

- `target/openapi-java-client-1.0.0.jar`
- `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.exiasoft.its.referencedata.*;
import com.exiasoft.its.referencedata.auth.*;
import com.exiasoft.its.referencedata.endpoint.rest.model.*;
import com.exiasoft.its.referencedata.endpoint.rest.api.CurrencyApi;

public class CurrencyApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/referencedata-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        CurrencyApi apiInstance = new CurrencyApi(defaultClient);
        CreateCurrencyRequest createCurrencyRequest = new CreateCurrencyRequest(); // CreateCurrencyRequest | Currency object to create
        try {
            Currency result = apiInstance.createCurrency(createCurrencyRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CurrencyApi#createCurrency");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8000/referencedata-service*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CurrencyApi* | [**createCurrency**](docs/CurrencyApi.md#createCurrency) | **POST** /v1/currencies | Create currency
*CurrencyApi* | [**findCurrency**](docs/CurrencyApi.md#findCurrency) | **GET** /v1/currencies | Find currency
*CurrencyApi* | [**getCurrency**](docs/CurrencyApi.md#getCurrency) | **GET** /v1/currencies/{currencyCode} | Get a single currency
*CurrencyApi* | [**putCurrency**](docs/CurrencyApi.md#putCurrency) | **PUT** /v1/currencies/{currencyCode} | Update a single currency


## Documentation for Models

 - [CreateCurrencyRequest](docs/CreateCurrencyRequest.md)
 - [CreateCurrencyRequestName](docs/CreateCurrencyRequestName.md)
 - [CreateCurrencyRequestShortName](docs/CreateCurrencyRequestShortName.md)
 - [Currency](docs/Currency.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [PageInformation](docs/PageInformation.md)
 - [PageOfCurrency](docs/PageOfCurrency.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### bearerAuth


- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



