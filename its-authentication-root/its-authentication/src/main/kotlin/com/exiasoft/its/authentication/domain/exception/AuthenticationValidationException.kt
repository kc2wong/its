package com.exiasoft.its.authentication.domain.exception

import com.exiasoft.its.common.exception.BaseException
import java.lang.Exception

class AuthenticationValidationException : BaseException {

    constructor(errorCode: String, errorParam: List<String>, exception: Exception?) : super(errorCode, errorParam, null, exception)
    constructor(errorCode: String) : super(errorCode)
    constructor(errorCode: String, errorParam: String) : this(errorCode, listOf(errorParam))
    constructor(errorCode: String, errorParam: List<String>) : super(errorCode, errorParam)

}