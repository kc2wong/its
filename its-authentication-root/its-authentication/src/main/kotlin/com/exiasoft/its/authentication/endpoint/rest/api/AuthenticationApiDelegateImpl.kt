package com.exiasoft.its.authentication.endpoint.rest.api

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.exiasoft.its.authentication.domain.exception.ErrorCode
import com.exiasoft.its.authentication.domain.exception.AuthenticationSystemException
import com.exiasoft.its.authentication.domain.exception.AuthenticationValidationException
import com.exiasoft.its.authentication.endpoint.rest.model.AccessTokenDto
import com.exiasoft.its.authentication.endpoint.rest.model.RefreshTokenDto
import com.exiasoft.its.authentication.endpoint.rest.model.SsoTokenDto
import com.exiasoft.its.common.util.DateTimeUtil
import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.nio.charset.Charset
import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.annotation.PostConstruct

@Component
class AuthenticationApiDelegateImpl(@Autowired private val applicationContext: ApplicationContext,
                                    @Autowired private val firebaseAuth: FirebaseAuth,
                                    @Autowired private val userManagementService: UserApi,
                                    @Value("\${jwt.key.private}") private val privateKeyPath: String,
                                    @Value("\${jwt.key.public}") private val publicKeyPath: String,
                                    @Value("\${jwt.refreshToken.expiryMinute}") private val refreshTokenExpiryMinute: Int,
                                    @Value("\${jwt.accessToken.expiryMinute}") private val accessTokenExpiryMinute: Int) : AuthenticationApiDelegate {

    companion object {
        const val ERROR_CODE_EXPIRED_ID_TOKEN = "EXPIRED_ID_TOKEN"
    }

    private val logger = LoggerFactory.getLogger(AuthenticationApiDelegateImpl::class.java)

    lateinit var privateKey : RSAPrivateKey
    lateinit var publicKey : RSAPublicKey
    lateinit var signAlgorithm: Algorithm
    lateinit var verifyAlgorithm: Algorithm

    @PostConstruct
    fun init() {
        applicationContext.getResource(privateKeyPath).inputStream.use {
            val privateKeyPem = String(it.readAllBytes(), Charset.defaultCharset())
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace(System.lineSeparator(), "")
                    .replace("-----END PRIVATE KEY-----", "")
            val src = Base64.getDecoder().decode(privateKeyPem)
            privateKey = KeyFactory.getInstance("RSA").generatePrivate(PKCS8EncodedKeySpec(src)) as RSAPrivateKey
        }
        applicationContext.getResource(publicKeyPath).inputStream.use {
            val publicKeyPem = String(it.readAllBytes(), Charset.defaultCharset())
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace(System.lineSeparator(), "")
                    .replace("-----END PUBLIC KEY-----", "")

            val src = Base64.getDecoder().decode(publicKeyPem)
            publicKey = KeyFactory.getInstance("RSA").generatePublic(X509EncodedKeySpec(src)) as RSAPublicKey
        }
        signAlgorithm = Algorithm.RSA256(null, privateKey)
        verifyAlgorithm = Algorithm.RSA256(publicKey, null)
    }

    override fun exchangeRefreshToken(ssoTokenDto: SsoTokenDto?): ResponseEntity<RefreshTokenDto> {
        ssoTokenDto ?.let {
            try {
                val firebaseToken = firebaseAuth.verifyIdToken(it.token)
                logger.info("verifyIdTokenSuccess")
                val now = Instant.now()
                val jwt = with(firebaseToken, {
                    JWT.create().withHeader(mapOf("token-type" to "refresh-token"))
                            .withSubject(this.uid)
//                            .withClaim("email", this.email)
//                            .withClaim("name", this.name)
                            .withIssuedAt(Date.from(now))
                            .withExpiresAt(Date.from(now.plus(refreshTokenExpiryMinute.toLong(), ChronoUnit.MINUTES)))
                            .sign(signAlgorithm)
                })
                return ResponseEntity.ok(RefreshTokenDto().token(jwt))
            }
            catch (ex: FirebaseAuthException) {
                logger.warn("Failed to verify idToken, errorCode = ${ex.authErrorCode}", ex)
                throw when (ex.authErrorCode.name) {
                    ERROR_CODE_EXPIRED_ID_TOKEN -> AuthenticationValidationException(ErrorCode.ERROR_INVALID_ID_TOKEN)
                    else -> AuthenticationSystemException(ex)
                }
            }
        }
        throw AuthenticationValidationException(ErrorCode.ERROR_MISSING_REQUIRED_FIELD, "ssoToken")
    }

    override fun exchangeAccessToken(authorization: String?): ResponseEntity<AccessTokenDto> {
        authorization ?.let {token ->
            val verifier = JWT.require(verifyAlgorithm).build()
            try {
                val jwt = if (token.toLowerCase().startsWith("bearer ")) token.substring(7) else token
                return with(verifier.verify(jwt), {

                    if (this.getHeaderClaim("token-type").asString() != "refresh-token") {
                        throw AuthenticationValidationException(ErrorCode.ERROR_INVALID_REFRESH_TOKEN)
                    }

                    val user = userManagementService.getUser(this.subject)
                    val now = DateTimeUtil.currentTime()
                    val builder = JWT.create().withHeader(mapOf("token-type" to "access-token"))
                            .withIssuedAt(Date.from(now))
                            .withExpiresAt(Date.from(now.plus(accessTokenExpiryMinute.toLong(), ChronoUnit.MINUTES)))
                    builder.withSubject(this.subject)
                    logger.debug("user = $user")
                    user ?.let {
                        builder.withClaim("email", it.email)
                        builder.withClaim("name", "${it.firstName} ${it.lastName}")
                    }
                    val jwt = builder.sign(signAlgorithm)
                    ResponseEntity.ok(AccessTokenDto().token(jwt))
                })
            }
            catch (ex: JWTVerificationException) {
                logger.warn("Failed to refreshToken", ex)
                throw AuthenticationValidationException(ErrorCode.ERROR_INVALID_REFRESH_TOKEN)
            }
        }
        throw AuthenticationValidationException(ErrorCode.ERROR_MISSING_REQUIRED_FIELD, "refreshToken")
    }

}