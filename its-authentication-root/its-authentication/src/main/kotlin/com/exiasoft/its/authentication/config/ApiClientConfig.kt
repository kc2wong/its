package com.exiasoft.its.authentication.config

import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class ApiClientConfig(@Value("\${service.endpoint.user-management}") private val userManagementServiceBaseUrl: String,
                      @Autowired private val userApi: UserApi) {

    @PostConstruct
    fun init() {
        userApi.apiClient.basePath = userManagementServiceBaseUrl
    }

}