package com.exiasoft.its.authentication.config

import com.exiasoft.its.authentication.domain.exception.ErrorCode
import com.exiasoft.its.common.exception.BaseException
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class ApplicationConfig() {

    @PostConstruct
    fun init() {
        BaseException.registerMessageProvider(ErrorCode::class.java)
    }

}