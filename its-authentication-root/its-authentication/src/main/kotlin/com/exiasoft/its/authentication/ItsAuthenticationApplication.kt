package com.exiasoft.its.authentication

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ItsAuthenticationApplication

fun main(args: Array<String>) {
	runApplication<ItsAuthenticationApplication>(*args)
}
