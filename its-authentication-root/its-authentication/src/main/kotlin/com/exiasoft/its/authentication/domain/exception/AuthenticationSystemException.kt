package com.exiasoft.its.authentication.domain.exception

class AuthenticationSystemException(message: String, cause: Exception) : Exception(message, cause) {
    constructor(cause: Exception) : this("System Error", cause)
}