package com.exiasoft.its.authentication.endpoint.rest.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T21:50:32.206260600+08:00[Asia/Shanghai]")

@Controller
@RequestMapping("${openapi.iTSAuthenticationServiceOpenAPI30.base-path:/authentication-service}")
public class AuthenticationApiController implements AuthenticationApi {

    private final AuthenticationApiDelegate delegate;

    public AuthenticationApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) AuthenticationApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new AuthenticationApiDelegate() {});
    }

    @Override
    public AuthenticationApiDelegate getDelegate() {
        return delegate;
    }

}
