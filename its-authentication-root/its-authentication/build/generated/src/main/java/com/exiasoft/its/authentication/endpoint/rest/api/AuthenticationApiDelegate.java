package com.exiasoft.its.authentication.endpoint.rest.api;

import com.exiasoft.its.authentication.endpoint.rest.model.AccessTokenDto;
import com.exiasoft.its.authentication.endpoint.rest.model.ErrorResponseDto;
import com.exiasoft.its.authentication.endpoint.rest.model.RefreshTokenDto;
import com.exiasoft.its.authentication.endpoint.rest.model.SsoTokenDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link AuthenticationApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T21:50:32.206260600+08:00[Asia/Shanghai]")

public interface AuthenticationApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /v1/access-tokens : Get access token
     * Exchange a access token with refresh token (in Authorization header)
     *
     * @param authorization  (required)
     * @return Successful operation (status code 200)
     *         or Input refresh token is invalid (status code 400)
     *         or Unexpected error occurrs (status code 500)
     * @see AuthenticationApi#exchangeAccessToken
     */
    default ResponseEntity<AccessTokenDto> exchangeAccessToken(String authorization) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"token\" : \"token\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /v1/refresh-tokens : Get refresh token
     * Exchange a refresh token after successfully login to identity provider
     *
     * @param ssoTokenDto SSO Token from identity provider (required)
     * @return Successful operation (status code 200)
     *         or Input access token is invalid (status code 400)
     *         or Unexpected error occurrs (status code 500)
     * @see AuthenticationApi#exchangeRefreshToken
     */
    default ResponseEntity<RefreshTokenDto> exchangeRefreshToken(SsoTokenDto ssoTokenDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"token\" : \"token\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
