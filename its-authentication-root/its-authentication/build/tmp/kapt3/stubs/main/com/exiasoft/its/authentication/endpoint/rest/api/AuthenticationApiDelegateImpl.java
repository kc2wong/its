package com.exiasoft.its.authentication.endpoint.rest.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0017\u0018\u0000 12\u00020\u0001:\u00011BK\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0001\u0010\b\u001a\u00020\t\u0012\b\b\u0001\u0010\n\u001a\u00020\t\u0012\b\b\u0001\u0010\u000b\u001a\u00020\f\u0012\b\b\u0001\u0010\r\u001a\u00020\f\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\'\u001a\b\u0012\u0004\u0012\u00020)0(2\b\u0010*\u001a\u0004\u0018\u00010\tH\u0016J\u0018\u0010+\u001a\b\u0012\u0004\u0012\u00020,0(2\b\u0010-\u001a\u0004\u0018\u00010.H\u0016J\b\u0010/\u001a\u000200H\u0017R\u000e\u0010\r\u001a\u00020\fX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u00020\u0013X\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\b\u001a\u00020\tX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u00020\u0019X\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u000e\u0010\n\u001a\u00020\tX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\u00020\u001fX\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u000e\u0010\u0006\u001a\u00020\u0007X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010$\u001a\u00020\u001fX\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010!\"\u0004\b&\u0010#\u00a8\u00062"}, d2 = {"Lcom/exiasoft/its/authentication/endpoint/rest/api/AuthenticationApiDelegateImpl;", "Lcom/exiasoft/its/authentication/endpoint/rest/api/AuthenticationApiDelegate;", "applicationContext", "Lorg/springframework/context/ApplicationContext;", "firebaseAuth", "Lcom/google/firebase/auth/FirebaseAuth;", "userManagementService", "Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApi;", "privateKeyPath", "", "publicKeyPath", "refreshTokenExpiryMinute", "", "accessTokenExpiryMinute", "(Lorg/springframework/context/ApplicationContext;Lcom/google/firebase/auth/FirebaseAuth;Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApi;Ljava/lang/String;Ljava/lang/String;II)V", "logger", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "privateKey", "Ljava/security/interfaces/RSAPrivateKey;", "getPrivateKey", "()Ljava/security/interfaces/RSAPrivateKey;", "setPrivateKey", "(Ljava/security/interfaces/RSAPrivateKey;)V", "publicKey", "Ljava/security/interfaces/RSAPublicKey;", "getPublicKey", "()Ljava/security/interfaces/RSAPublicKey;", "setPublicKey", "(Ljava/security/interfaces/RSAPublicKey;)V", "signAlgorithm", "Lcom/auth0/jwt/algorithms/Algorithm;", "getSignAlgorithm", "()Lcom/auth0/jwt/algorithms/Algorithm;", "setSignAlgorithm", "(Lcom/auth0/jwt/algorithms/Algorithm;)V", "verifyAlgorithm", "getVerifyAlgorithm", "setVerifyAlgorithm", "exchangeAccessToken", "Lorg/springframework/http/ResponseEntity;", "Lcom/exiasoft/its/authentication/endpoint/rest/model/AccessTokenDto;", "authorization", "exchangeRefreshToken", "Lcom/exiasoft/its/authentication/endpoint/rest/model/RefreshTokenDto;", "ssoTokenDto", "Lcom/exiasoft/its/authentication/endpoint/rest/model/SsoTokenDto;", "init", "", "Companion", "its-authentication"})
@org.springframework.stereotype.Component()
public class AuthenticationApiDelegateImpl implements com.exiasoft.its.authentication.endpoint.rest.api.AuthenticationApiDelegate {
    private final org.slf4j.Logger logger = null;
    @org.jetbrains.annotations.NotNull()
    public java.security.interfaces.RSAPrivateKey privateKey;
    @org.jetbrains.annotations.NotNull()
    public java.security.interfaces.RSAPublicKey publicKey;
    @org.jetbrains.annotations.NotNull()
    public com.auth0.jwt.algorithms.Algorithm signAlgorithm;
    @org.jetbrains.annotations.NotNull()
    public com.auth0.jwt.algorithms.Algorithm verifyAlgorithm;
    private final org.springframework.context.ApplicationContext applicationContext = null;
    private final com.google.firebase.auth.FirebaseAuth firebaseAuth = null;
    private final com.exiasoft.its.usermgt.endpoint.rest.api.UserApi userManagementService = null;
    private final java.lang.String privateKeyPath = null;
    private final java.lang.String publicKeyPath = null;
    private final int refreshTokenExpiryMinute = 0;
    private final int accessTokenExpiryMinute = 0;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_CODE_EXPIRED_ID_TOKEN = "EXPIRED_ID_TOKEN";
    public static final com.exiasoft.its.authentication.endpoint.rest.api.AuthenticationApiDelegateImpl.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public java.security.interfaces.RSAPrivateKey getPrivateKey() {
        return null;
    }
    
    public void setPrivateKey(@org.jetbrains.annotations.NotNull()
    java.security.interfaces.RSAPrivateKey p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public java.security.interfaces.RSAPublicKey getPublicKey() {
        return null;
    }
    
    public void setPublicKey(@org.jetbrains.annotations.NotNull()
    java.security.interfaces.RSAPublicKey p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.auth0.jwt.algorithms.Algorithm getSignAlgorithm() {
        return null;
    }
    
    public void setSignAlgorithm(@org.jetbrains.annotations.NotNull()
    com.auth0.jwt.algorithms.Algorithm p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.auth0.jwt.algorithms.Algorithm getVerifyAlgorithm() {
        return null;
    }
    
    public void setVerifyAlgorithm(@org.jetbrains.annotations.NotNull()
    com.auth0.jwt.algorithms.Algorithm p0) {
    }
    
    @javax.annotation.PostConstruct()
    public void init() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.authentication.endpoint.rest.model.RefreshTokenDto> exchangeRefreshToken(@org.jetbrains.annotations.Nullable()
    com.exiasoft.its.authentication.endpoint.rest.model.SsoTokenDto ssoTokenDto) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.authentication.endpoint.rest.model.AccessTokenDto> exchangeAccessToken(@org.jetbrains.annotations.Nullable()
    java.lang.String authorization) {
        return null;
    }
    
    public AuthenticationApiDelegateImpl(@org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    org.springframework.context.ApplicationContext applicationContext, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.google.firebase.auth.FirebaseAuth firebaseAuth, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.exiasoft.its.usermgt.endpoint.rest.api.UserApi userManagementService, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Value(value = "${jwt.key.private}")
    java.lang.String privateKeyPath, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Value(value = "${jwt.key.public}")
    java.lang.String publicKeyPath, @org.springframework.beans.factory.annotation.Value(value = "${jwt.refreshToken.expiryMinute}")
    int refreshTokenExpiryMinute, @org.springframework.beans.factory.annotation.Value(value = "${jwt.accessToken.expiryMinute}")
    int accessTokenExpiryMinute) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/exiasoft/its/authentication/endpoint/rest/api/AuthenticationApiDelegateImpl$Companion;", "", "()V", "ERROR_CODE_EXPIRED_ID_TOKEN", "", "its-authentication"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}