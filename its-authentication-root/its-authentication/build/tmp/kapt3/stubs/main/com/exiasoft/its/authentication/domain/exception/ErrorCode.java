package com.exiasoft.its.authentication.domain.exception;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/exiasoft/its/authentication/domain/exception/ErrorCode;", "", "()V", "Companion", "its-authentication"})
public final class ErrorCode {
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Missing required field %s")
    public static final java.lang.String ERROR_MISSING_REQUIRED_FIELD = "AUTHEN.10001";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "User with id %s already exists")
    public static final java.lang.String ERROR_USER_ID_ALREADY_EXISTS = "AUTHEN.00001";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "User with email %s already exists")
    public static final java.lang.String ERROR_USER_EMAIL_ALREADY_EXISTS = "AUTHEN.00002";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "User with id %s not found")
    public static final java.lang.String ERROR_USER_ID_NOT_FOUND = "AUTHEN.00003";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Incorrect version number of User %s")
    public static final java.lang.String ERROR_INCORRECT_USER_VERSION = "AUTHEN.00004";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Missing search criteria")
    public static final java.lang.String ERROR_MIISSING_SEARCH_CRITERIA = "AUTHEN.00005";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Invalid search criteria %s : %s")
    public static final java.lang.String ERROR_INVALID_SEARCH_CRITERIA = "AUTHEN.00006";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "SSO Token is expired")
    public static final java.lang.String ERROR_INVALID_ID_TOKEN = "AUTHEN.00010";
    @org.jetbrains.annotations.NotNull()
    @com.exiasoft.its.common.exception.ErrorMessage(message = "Invalid refresh token")
    public static final java.lang.String ERROR_INVALID_REFRESH_TOKEN = "AUTHEN.00011";
    public static final com.exiasoft.its.authentication.domain.exception.ErrorCode.Companion Companion = null;
    
    public ErrorCode() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/exiasoft/its/authentication/domain/exception/ErrorCode$Companion;", "", "()V", "ERROR_INCORRECT_USER_VERSION", "", "ERROR_INVALID_ID_TOKEN", "ERROR_INVALID_REFRESH_TOKEN", "ERROR_INVALID_SEARCH_CRITERIA", "ERROR_MIISSING_SEARCH_CRITERIA", "ERROR_MISSING_REQUIRED_FIELD", "ERROR_USER_EMAIL_ALREADY_EXISTS", "ERROR_USER_ID_ALREADY_EXISTS", "ERROR_USER_ID_NOT_FOUND", "its-authentication"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}