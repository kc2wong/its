package com.exiasoft.its.authentication.config;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/exiasoft/its/authentication/config/ApiClientConfig;", "", "userManagementServiceBaseUrl", "", "userApi", "Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApi;", "(Ljava/lang/String;Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApi;)V", "init", "", "its-authentication"})
@org.springframework.context.annotation.Configuration()
public class ApiClientConfig {
    private final java.lang.String userManagementServiceBaseUrl = null;
    private final com.exiasoft.its.usermgt.endpoint.rest.api.UserApi userApi = null;
    
    @javax.annotation.PostConstruct()
    public void init() {
    }
    
    public ApiClientConfig(@org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Value(value = "${service.endpoint.user-management}")
    java.lang.String userManagementServiceBaseUrl, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.exiasoft.its.usermgt.endpoint.rest.api.UserApi userApi) {
        super();
    }
}