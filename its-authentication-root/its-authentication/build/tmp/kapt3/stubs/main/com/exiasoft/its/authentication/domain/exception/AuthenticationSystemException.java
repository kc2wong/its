package com.exiasoft.its.authentication.domain.exception;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0013\b\u0016\u0012\n\u0010\u0003\u001a\u00060\u0001j\u0002`\u0002\u00a2\u0006\u0002\u0010\u0004B\u0019\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\u0010\u0003\u001a\u00060\u0001j\u0002`\u0002\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/exiasoft/its/authentication/domain/exception/AuthenticationSystemException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "cause", "(Ljava/lang/Exception;)V", "message", "", "(Ljava/lang/String;Ljava/lang/Exception;)V", "its-authentication"})
public final class AuthenticationSystemException extends java.lang.Exception {
    
    public AuthenticationSystemException(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.Exception cause) {
        super();
    }
    
    public AuthenticationSystemException(@org.jetbrains.annotations.NotNull()
    java.lang.Exception cause) {
        super();
    }
}