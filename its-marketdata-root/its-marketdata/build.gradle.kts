import org.jetbrains.kotlin.gradle.plugin.sources.checkSourceSetVisibilityRequirements
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.3.4.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	id("org.openapi.generator") version "4.3.1"
	id("maven")
	kotlin("jvm") version "1.3.72"
	kotlin("kapt") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"
}

group = "com.exiasoft.its"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenLocal()
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("javax.validation:validation-api:2.0.1.Final")
	implementation("io.springfox:springfox-swagger2:2.8.0")
	implementation("org.openapitools:jackson-databind-nullable:0.2.1")
	implementation("javax.xml.bind:jaxb-api:2.2.11")

	implementation("com.google.firebase:firebase-admin:7.0.1")
	implementation("com.auth0:java-jwt:3.11.0")

	implementation("org.mapstruct:mapstruct:1.4.1.Final")
	kapt("org.mapstruct:mapstruct-processor:1.4.1.Final")

	implementation("com.exiasoft.its:its-common:$version")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		implementation("io.springfox:springfox-swagger2:2.8.0")
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
//		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.getByName<Task>("compileJava") {
	dependsOn("openApiGenerate")
}

tasks.getByName<Task>("compileKotlin") {
	dependsOn("openApiGenerate")
}

tasks.getByName<Task>("install") {
	dependsOn("kotlinSourcesJar")
}

sourceSets {
	main {
		java.srcDirs("$buildDir/generated/src/main/java", "$buildDir/generated/source/kapt/main")
	}
}

openApiGenerate {
	generatorName.set("spring")
	inputSpec.set("$rootDir/its-marketdata/src/main/resources/static/api-spec.yaml")
	outputDir.set("$buildDir/generated")
	apiPackage.set("com.exiasoft.its.marketdata.endpoint.rest.api")
	modelPackage.set("com.exiasoft.its.marketdata.endpoint.rest.model")
	invokerPackage.set("com.exiasoft.its.marketdata")
	ignoreFileOverride.set("$rootDir/its-marketdata/src/main/resources/.openapi-generator-ignore")
	modelNameSuffix.set("Dto")
	configOptions.set(mapOf(
			"dateLibrary" to "java8",
			"delegatePattern" to "true",
			"useTags" to "true",
			"apiDocs" to "false",
			"generateSupportingFiles" to "false"
	))
}