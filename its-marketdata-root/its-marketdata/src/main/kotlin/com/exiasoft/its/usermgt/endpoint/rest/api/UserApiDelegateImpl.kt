package com.exiasoft.its.usermgt.endpoint.rest.api

import com.exiasoft.its.usermgt.domain.UserManagementService
import com.exiasoft.its.usermgt.domain.model.User
import com.exiasoft.its.usermgt.endpoint.rest.mapper.UserDtoMapper
import com.exiasoft.its.usermgt.endpoint.rest.model.CreateUserRequestDto
import com.exiasoft.its.usermgt.endpoint.rest.model.UserDto
import com.exiasoft.its.usermgt.endpoint.rest.model.UserSearchResultDto
import com.exiasoft.itscommon.bean.RequestData
import org.mapstruct.factory.Mappers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component
class UserApiDelegateImpl(val userManagementService: UserManagementService) : UserApiDelegate {
    val mapper = Mappers.getMapper(UserDtoMapper::class.java)

    @Autowired
    lateinit var requestData: RequestData

    override fun createUser(createUserRequestDto: CreateUserRequestDto?): ResponseEntity<UserDto> {
        val user = with(createUserRequestDto!!, {
            User(this.userid, this.firstName, this.lastName, this.email)
        })
        val rtn = userManagementService.createUser(user, requestData.userProfile)
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.domain2Dto(rtn))
    }

    override fun findUser(userid: String?, email: String?, firstName: String?, lastName: String?, limit: Int?, sort: String?): ResponseEntity<UserSearchResultDto> {
        val result = userManagementService.findUser(userid, email, firstName, lastName, limit ?: 25, sort)
        val rtn = UserSearchResultDto()
        rtn.data = result.first.map { mapper.domain2Dto(it) }
        rtn.hasMore = result.second
        return ResponseEntity.ok(rtn)
    }

    override fun getUser(userid: String?): ResponseEntity<UserDto> {
        return userManagementService.getUser(userid!!) ?.let {
            ResponseEntity.ok(mapper.domain2Dto(it))
        } ?: ResponseEntity.notFound().build()
    }

    override fun putUser(userid: String?, userDto: UserDto?): ResponseEntity<UserDto> {
        val user = mapper.dto2Domain(userDto!!)
        val rtn = userManagementService.updateUser(user, requestData.userProfile)
        return ResponseEntity.ok().body(mapper.domain2Dto(rtn))
    }

}