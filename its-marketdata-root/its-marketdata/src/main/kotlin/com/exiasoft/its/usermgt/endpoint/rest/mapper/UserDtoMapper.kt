package com.exiasoft.its.usermgt.endpoint.rest.mapper

import com.exiasoft.its.usermgt.domain.model.User
import com.exiasoft.its.usermgt.domain.model.UserStatus
import com.exiasoft.its.usermgt.endpoint.rest.model.UserDto
import com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.Named

@Mapper(uses = [BaseDtoMapper::class])
interface UserDtoMapper {

    @Mappings(value = [
        Mapping(source = "status", target = "status", qualifiedByName = ["toUserStatus"])
    ])
    fun dto2Domain(userDto: UserDto) : User

    @Mappings(value = [
        Mapping(source = "status", target = "status", qualifiedByName = ["toUserStatusDto"])
    ])
    fun domain2Dto(user: User) : UserDto

    @Named("toUserStatus")
    fun dto2Domain(userStatusDto: UserStatusDto) : UserStatus {
        return when (userStatusDto) {
            UserStatusDto.ACTIVE -> UserStatus.ACTIVE
            UserStatusDto.INACTIVE -> UserStatus.INACTIVE
            UserStatusDto.SUSPEND -> UserStatus.SUSPEND
        }
    }

    @Named("toUserStatusDto")
    fun domain2Dto(userStatus: UserStatus) : UserStatusDto {
        return when (userStatus) {
            UserStatus.ACTIVE -> UserStatusDto.ACTIVE
            UserStatus.INACTIVE -> UserStatusDto.INACTIVE
            UserStatus.SUSPEND -> UserStatusDto.SUSPEND
        }
    }

}