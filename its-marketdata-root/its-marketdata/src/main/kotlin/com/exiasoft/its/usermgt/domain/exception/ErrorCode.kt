package com.exiasoft.its.usermgt.domain.exception

import com.exiasoft.itscommon.exception.ErrorMessage
import com.exiasoft.itscommon.exception.ErrorMessageProvider

class ErrorCode: ErrorMessageProvider {
    companion object {

        @ErrorMessage(message = "Missing required field %s")
        const val ERROR_MISSING_REQUIRED_FIELD = "AUTHEN.10001"

        @ErrorMessage(message = "User with id %s already exists")
        const val ERROR_USER_ID_ALREADY_EXISTS = "AUTHEN.00001"

        @ErrorMessage(message = "User with email %s already exists")
        const val ERROR_USER_EMAIL_ALREADY_EXISTS = "AUTHEN.00002"

        @ErrorMessage(message = "User with id %s not found")
        const val ERROR_USER_ID_NOT_FOUND = "AUTHEN.00003"

        @ErrorMessage(message = "Incorrect version number of User %s")
        const val ERROR_INCORRECT_USER_VERSION = "AUTHEN.00004"

        @ErrorMessage(message = "Missing search criteria")
        const val ERROR_MIISSING_SEARCH_CRITERIA = "AUTHEN.00005"

        @ErrorMessage(message = "Invalid search criteria %s : %s")
        const val ERROR_INVALID_SEARCH_CRITERIA = "AUTHEN.00006"

        @ErrorMessage(message = "SSO Token is expired")
        const val ERROR_INVALID_ID_TOKEN = "AUTHEN.00010"

        @ErrorMessage(message = "Invalid refresh token")
        const val ERROR_INVALID_REFRESH_TOKEN = "AUTHEN.00011"

        @ErrorMessage(message = "System error")
        const val ERROR_SYSTEM_ERROR = "AUTHEN.9999"

    }
}