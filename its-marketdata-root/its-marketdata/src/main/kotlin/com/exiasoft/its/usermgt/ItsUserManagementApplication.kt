package com.exiasoft.its.usermgt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ItsUserManagementApplication

fun main(args: Array<String>) {
	runApplication<ItsUserManagementApplication>(*args)
}
