package com.exiasoft.its.usermgt.config

import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.cloud.FirestoreClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct


@Configuration
class FirebaseConfig(@Autowired private val applicationContext: ApplicationContext,
                     @Value("\${firebase.database.url}") val firebaseDatabaseUrl: String,
                     @Value("\${firebase.service.account.credential}") val firebaseServiceAccountCredential: String) {

    private val logger: Logger = LoggerFactory.getLogger(FirebaseConfig::class.java)

    @PostConstruct
    fun firebaseInit() {
        logger.info("Initialize firebase with url = {}, credential = {}", firebaseDatabaseUrl, firebaseServiceAccountCredential)
        applicationContext.getResource(firebaseServiceAccountCredential).inputStream.use {
            val options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(it))
                    .setDatabaseUrl(firebaseDatabaseUrl)
                    .build()
            FirebaseApp.initializeApp(options)
        }
    }

    @Bean
    fun firebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Bean
    fun fireStore(): Firestore {
        return FirestoreClient.getFirestore()
    }

}