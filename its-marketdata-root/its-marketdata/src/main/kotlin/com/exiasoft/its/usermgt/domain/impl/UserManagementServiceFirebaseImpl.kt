package com.exiasoft.its.usermgt.domain.impl

import com.exiasoft.its.usermgt.domain.UserManagementService
import com.exiasoft.its.usermgt.domain.exception.ErrorCode
import com.exiasoft.its.usermgt.domain.model.User
import com.exiasoft.its.usermgt.domain.model.UserStatus
import com.exiasoft.itscommon.bean.UserProfile
import com.exiasoft.itscommon.exception.SystemException
import com.exiasoft.itscommon.exception.ValidationException
import com.google.cloud.firestore.DocumentSnapshot
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.Query
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.UserRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class UserManagementServiceFirebaseImpl(@Autowired val firebaseAuth: FirebaseAuth,
                                        @Autowired val fireStore: Firestore) : UserManagementService {

    companion object {
        const val FB_ERROR_CODE_USER_NOT_FOUND = "USER_NOT_FOUND"
        const val FB_ERROR_CODE_UID_ALREADY_EXISTS = "UID_ALREADY_EXISTS"
        const val FB_ERROR_CODE_EMAIL_ALREADY_EXISTS = "EMAIL_ALREADY_EXISTS"
    }

    private val logger = LoggerFactory.getLogger(UserManagementServiceFirebaseImpl::class.java)

    @Throws(exceptionClasses = [ValidationException::class, SystemException::class])
    override fun createUser(user: User, invocationUser: UserProfile): User {

        val userId = user.userid.toLowerCase()

        // Check uid exists or not
        if (getUserByUserid(userId) != null) {
            throw ValidationException(ErrorCode.ERROR_USER_ID_ALREADY_EXISTS, user.userid)
        }

        // Check email exists or not
        if (getUserByEmail(user.email) != null) {
            throw ValidationException(ErrorCode.ERROR_USER_EMAIL_ALREADY_EXISTS, user.email)
        }

        // Create user
        val request = UserRecord.CreateRequest()
        request.setUid(userId)
        request.setEmail(user.email)
        request.setEmailVerified(false)
        request.setDisplayName("$user.firstName $user.lastName")
        request.setDisabled(false)
        try {
            firebaseAuth.createUser(request)
        }
        catch (e : FirebaseAuthException) {
            logger.info("Error in creating user in firebase, code = {}", e.authErrorCode)
            throw when (e.authErrorCode.name) {
                FB_ERROR_CODE_UID_ALREADY_EXISTS -> ValidationException(ErrorCode.ERROR_USER_ID_ALREADY_EXISTS, user.userid)
                FB_ERROR_CODE_EMAIL_ALREADY_EXISTS -> ValidationException(ErrorCode.ERROR_USER_EMAIL_ALREADY_EXISTS, user.email)
                else -> SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
            }
        }

        // Update database
        val now = Instant.now()
        val docRef = fireStore.collection("users").document(userId)
        user.createdBy = invocationUser.userid
        user.createdDateTime = now
        user.updatedBy = invocationUser.userid
        user.updatedDateTime = now
        user.version = 1
        user.status = UserStatus.ACTIVE
        docRef.set(userToUserData(user)).get()

        return user
    }

    @Throws(exceptionClasses = [ValidationException::class, SystemException::class])
    override fun updateUser(user: User, invocationUser: UserProfile): User {
        val userId = user.userid.toLowerCase()

        // Check uid exists or not
        return getUserByUserid(userId) ?.let {

            if (it.email != user.email && getUserByEmail(user.email) != null) {
                throw ValidationException(ErrorCode.ERROR_USER_EMAIL_ALREADY_EXISTS, user.email)
            }

            val docRef = fireStore.collection("users").document(userId)
            if (docRef.get().get().getLong("version") != user.version) {
                throw ValidationException(ErrorCode.ERROR_INCORRECT_USER_VERSION, user.userid)
            }

            val request = UserRecord.UpdateRequest(userId)
            request.setEmail(user.email)
            request.setEmailVerified(false)
            request.setDisplayName("$user.firstName $user.lastName")
            request.setDisabled(user.status == UserStatus.ACTIVE)
            try {
                firebaseAuth.updateUser(request)
            }
            catch (e : FirebaseAuthException) {
                logger.info("Error in updating user in firebase, code = {}", e.authErrorCode)
                throw when (e.authErrorCode.name) {
                    FB_ERROR_CODE_EMAIL_ALREADY_EXISTS -> ValidationException(ErrorCode.ERROR_USER_EMAIL_ALREADY_EXISTS, user.email)
                    else -> SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
                }
            }

            // Update database
            val now = Instant.now()
            user.updatedBy = invocationUser.userid
            user.updatedDateTime = now
            user.version = user.version.inc()
            docRef.update(userToUserData(user)).get()

            return user
        } ?: throw throw ValidationException(ErrorCode.ERROR_USER_ID_NOT_FOUND, user.userid)
    }

    override fun getUser(userId: String): User? {
        return getUserByUserid(userId)?.let { userRecord ->
            fireStore.collection("users")
                    .document(userRecord.uid)
                    .get().get()?.let { documentToUser(it) }
        }
    }

    override fun findUser(userId: String?, email: String?, firstName: String?, lastName: String?, limit: Int, sort: String? ): Pair<List<User>, Boolean> {

        if (limit <= 0) {
            throw ValidationException(ErrorCode.ERROR_INVALID_SEARCH_CRITERIA, listOf("limit", limit.toString()))
        }

        val root = fireStore.collection("users")
        var query: Query = root
        email?.let { query = query.whereEqualTo("emailLower", it) }
        firstName?.let { query = query.whereEqualTo("firstNameLower", it) }
        lastName?.let { query = query.whereEqualTo("lastNameLower", it) }

        if (root == query) {
            // no other criteria
            if (userId == null) {
                throw ValidationException(ErrorCode.ERROR_MIISSING_SEARCH_CRITERIA)
            } else {
                val user = getUser(userId)
                return Pair(user?.let { listOf(it) } ?: emptyList(), false)
            }
        }

        sort?.let { s ->
            s.split(",").forEach {
                if (it.startsWith("-")) {
                    query.orderBy(it.substring(1), Query.Direction.DESCENDING)
                }
                else if (it.startsWith("+")) {
                    query.orderBy(it.substring(1), Query.Direction.ASCENDING)
                }
                else {
                    query.orderBy(it, Query.Direction.ASCENDING)
                }
            }
        }

        query.limit(limit + 1)
        val result = query.get().get().documents
                .filter { userId == null || it.id.toLowerCase() == userId.toLowerCase() }
                .map { documentToUser(it) }
        return Pair(if (result.size > limit) result.subList(0, limit) else result, result.size > limit)
    }

    @Throws(SystemException::class)
    private fun getUserByEmail(email: String) : UserRecord? {
        // Check email exists or not
        try {
            return firebaseAuth.getUserByEmail(email.toLowerCase())
        }
        catch (e : FirebaseAuthException) {
            if (FB_ERROR_CODE_USER_NOT_FOUND != e.authErrorCode.name) {
                logger.info("Error in getting user from firebase, error code = {}", e.authErrorCode)
                throw SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
            }

            try {
                return firebaseAuth.getUserByEmail(email)
            }
            catch (e : FirebaseAuthException) {
                if (FB_ERROR_CODE_USER_NOT_FOUND != e.authErrorCode.name) {
                    logger.info("Error in getting user from firebase, error code = {}", e.authErrorCode)
                    throw SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
                }
            }
        }
        return null
    }

    @Throws(SystemException::class)
    private fun getUserByUserid(userid: String) : UserRecord? {
        try {
            // For managed users, the userid should be in lower case.  However for userid generated by firebase, it can contains upper case
            val rtn = firebaseAuth.getUser(userid.toLowerCase())
            return rtn
        }
        catch (e : FirebaseAuthException) {
            if (FB_ERROR_CODE_USER_NOT_FOUND != e.authErrorCode.name) {
                logger.info("Error in getting user from firebase, error code = {}", e.authErrorCode)
                throw SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
            }

            try {
                // For managed users, the userid should be in lower case.  However for userid generated by firebase, it can contains upper case
                val rtn = firebaseAuth.getUser(userid)
                return rtn
            }
            catch (e : FirebaseAuthException) {
                if (FB_ERROR_CODE_USER_NOT_FOUND != e.authErrorCode.name) {
                    logger.info("Error in getting user from firebase, error code = {}", e.authErrorCode)
                    throw SystemException(ErrorCode.ERROR_SYSTEM_ERROR, e)
                }
            }
        }
        return null
    }

    private fun userToUserData(user: User) : Map<String, Any> {
        return mapOf<String, Any>("firstName" to user.firstName, "firstNameLower" to user.firstName.toLowerCase(),
                "lastName" to user.lastName, "lastNameLower" to user.lastName.toLowerCase(),
                "email" to user.email, "emailLower" to user.email.toLowerCase(),
                "status" to user.status.value,
                "createdBy" to user.createdBy, "createdDateTime" to user.createdDateTime.toString(),
                "updatedBy" to user.updatedBy, "updatedDateTime" to user.updatedDateTime.toString(),
                "version" to user.version)
    }

    private fun documentToUser(documentSnapshot : DocumentSnapshot) : User {
        val now = Instant.now()
        with (documentSnapshot, {
            val user = User(this.id, this.getString("firstName") ?: "", this.getString("lastName") ?: "", this.getString("email") ?: "")
            user.createdBy = this.getString("createdBy") ?: ""
            user.createdDateTime = this.getString("createdDateTime") ?.let { Instant.parse(it) } ?: now
            user.updatedBy = this.getString("updatedBy") ?: ""
            user.updatedDateTime = this.getString("updatedDateTime") ?.let { Instant.parse(it) } ?: now
            user.version = this.getLong("version") ?: 1
            user.status = this.getString("status") ?.let { UserStatus.valueOf(it) } ?: UserStatus.ACTIVE
            return user
        })
    }
}