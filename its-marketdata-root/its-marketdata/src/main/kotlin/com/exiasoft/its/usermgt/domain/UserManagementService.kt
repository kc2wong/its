package com.exiasoft.its.usermgt.domain

import com.exiasoft.its.usermgt.domain.model.User
import com.exiasoft.itscommon.bean.UserProfile

interface UserManagementService {

    fun getUser(userId: String) : User?

    fun findUser(userId: String?, email: String?, firstName: String?, lastName: String?, limit: Int, sort: String?) : Pair<List<User>, Boolean>

    fun createUser(user: User, invocationUser: UserProfile) : User

    fun updateUser(user: User, invocationUser: UserProfile) : User

}