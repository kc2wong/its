

# PageSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **Integer** | Starting record number (from 1) in matched result |  [optional]
**end** | **Integer** | Ending record number in matched result |  [optional]
**total** | **Integer** | Total number of matched record |  [optional]



