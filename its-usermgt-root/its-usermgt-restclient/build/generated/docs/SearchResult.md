

# SearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hasMore** | **Boolean** | Indicate there is more record that matches input search criteria | 



