

# User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdBy** | **String** | Userid of creator | 
**createdDateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**updatedBy** | **String** | Userid of last updater | 
**updatedDateTime** | [**OffsetDateTime**](OffsetDateTime.md) |  | 
**version** | [**BigDecimal**](BigDecimal.md) |  | 
**status** | [**UserStatus**](UserStatus.md) |  | 



