# UserApi

All URIs are relative to *http://localhost:8000/usermgt-service*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UserApi.md#createUser) | **POST** /v1/users | Create user
[**findUser**](UserApi.md#findUser) | **GET** /v1/users | Find users
[**getUser**](UserApi.md#getUser) | **GET** /v1/users/{userid} | Get a single user
[**putUser**](UserApi.md#putUser) | **PUT** /v1/users/{userid} | Update a single user



## createUser

> User createUser(createUserRequest)

Create user

This can only be done by the logged in user.

### Example

```java
// Import classes:
import com.exiasoft.its.usermgt.ApiClient;
import com.exiasoft.its.usermgt.ApiException;
import com.exiasoft.its.usermgt.Configuration;
import com.exiasoft.its.usermgt.auth.*;
import com.exiasoft.its.usermgt.models.*;
import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/usermgt-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        UserApi apiInstance = new UserApi(defaultClient);
        CreateUserRequest createUserRequest = new CreateUserRequest(); // CreateUserRequest | User object to create
        try {
            User result = apiInstance.createUser(createUserRequest);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#createUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserRequest** | [**CreateUserRequest**](CreateUserRequest.md)| User object to create |

### Return type

[**User**](User.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Successful operation |  -  |
| **400** | Input information is invalid |  -  |
| **403** | Input access token is expired |  -  |
| **500** | Unexpected error occurrs |  -  |


## findUser

> UserSearchResult findUser(userid, email, firstName, lastName, limit, sort)

Find users

Find users by criteria

### Example

```java
// Import classes:
import com.exiasoft.its.usermgt.ApiClient;
import com.exiasoft.its.usermgt.ApiException;
import com.exiasoft.its.usermgt.Configuration;
import com.exiasoft.its.usermgt.auth.*;
import com.exiasoft.its.usermgt.models.*;
import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/usermgt-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        UserApi apiInstance = new UserApi(defaultClient);
        String userid = "userid_example"; // String | Find user with matched userid (exact match)
        String email = "email_example"; // String | Find user with matched email (exact match,  case insensitive)
        String firstName = "firstName_example"; // String | Find user with matched first name (exact match, case insensitive)
        String lastName = "lastName_example"; // String | Find user with matched last name (exact match, case insensitive)
        Integer limit = 25; // Integer | Maximum number of records to return.  Default is 25
        String sort = lastName,-email; // String | Comma separated sorting criteria.  To sort in descending order, add \"-\" before the field.  E.g lastName,-email
        try {
            UserSearchResult result = apiInstance.findUser(userid, email, firstName, lastName, limit, sort);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#findUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userid** | **String**| Find user with matched userid (exact match) | [optional]
 **email** | **String**| Find user with matched email (exact match,  case insensitive) | [optional]
 **firstName** | **String**| Find user with matched first name (exact match, case insensitive) | [optional]
 **lastName** | **String**| Find user with matched last name (exact match, case insensitive) | [optional]
 **limit** | **Integer**| Maximum number of records to return.  Default is 25 | [optional] [default to 25]
 **sort** | **String**| Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email | [optional] [default to &quot;userid&quot;]

### Return type

[**UserSearchResult**](UserSearchResult.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Input search criteria is invalid |  -  |
| **403** | Input access token is expired |  -  |
| **500** | Unexpected error occurrs |  -  |


## getUser

> User getUser(userid)

Get a single user

Get a single user by userid

### Example

```java
// Import classes:
import com.exiasoft.its.usermgt.ApiClient;
import com.exiasoft.its.usermgt.ApiException;
import com.exiasoft.its.usermgt.Configuration;
import com.exiasoft.its.usermgt.auth.*;
import com.exiasoft.its.usermgt.models.*;
import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/usermgt-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        UserApi apiInstance = new UserApi(defaultClient);
        String userid = "userid_example"; // String | Id of the user to get
        try {
            User result = apiInstance.getUser(userid);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#getUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userid** | **String**| Id of the user to get |

### Return type

[**User**](User.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **403** | Input access token is expired |  -  |
| **404** | No user is found with input userid |  -  |
| **500** | Unexpected error occurrs |  -  |


## putUser

> User putUser(userid, user)

Update a single user

Update a single user by userid

### Example

```java
// Import classes:
import com.exiasoft.its.usermgt.ApiClient;
import com.exiasoft.its.usermgt.ApiException;
import com.exiasoft.its.usermgt.Configuration;
import com.exiasoft.its.usermgt.auth.*;
import com.exiasoft.its.usermgt.models.*;
import com.exiasoft.its.usermgt.endpoint.rest.api.UserApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost:8000/usermgt-service");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        UserApi apiInstance = new UserApi(defaultClient);
        String userid = "userid_example"; // String | Id of the user to update
        User user = new User(); // User | User object to update
        try {
            User result = apiInstance.putUser(userid, user);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling UserApi#putUser");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userid** | **String**| Id of the user to update |
 **user** | [**User**](User.md)| User object to update |

### Return type

[**User**](User.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Input information is invalid |  -  |
| **403** | Input access token is expired |  -  |
| **404** | No user is found with input userid |  -  |
| **500** | Unexpected error occurrs |  -  |

