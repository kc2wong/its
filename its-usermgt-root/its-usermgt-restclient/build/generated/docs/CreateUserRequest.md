

# CreateUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userid** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**email** | **String** |  | 



