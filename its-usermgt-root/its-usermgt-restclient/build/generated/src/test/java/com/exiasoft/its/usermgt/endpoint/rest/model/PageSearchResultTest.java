/*
 * ITS User Management Service - OpenAPI 3.0
 * This is a sample User Management Service based on the OpenAPI 3.0 specification
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.exiasoft.its.usermgt.endpoint.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for PageSearchResult
 */
public class PageSearchResultTest {
    private final PageSearchResult model = new PageSearchResult();

    /**
     * Model tests for PageSearchResult
     */
    @Test
    public void testPageSearchResult() {
        // TODO: test PageSearchResult
    }

    /**
     * Test the property 'start'
     */
    @Test
    public void startTest() {
        // TODO: test start
    }

    /**
     * Test the property 'end'
     */
    @Test
    public void endTest() {
        // TODO: test end
    }

    /**
     * Test the property 'total'
     */
    @Test
    public void totalTest() {
        // TODO: test total
    }

}
