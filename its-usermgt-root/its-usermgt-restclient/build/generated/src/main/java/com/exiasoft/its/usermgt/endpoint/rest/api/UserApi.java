package com.exiasoft.its.usermgt.endpoint.rest.api;

import com.exiasoft.its.usermgt.ApiClient;

import com.exiasoft.its.usermgt.endpoint.rest.model.CreateUserRequest;
import com.exiasoft.its.usermgt.endpoint.rest.model.ErrorResponse;
import com.exiasoft.its.usermgt.endpoint.rest.model.User;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserSearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-11-05T00:05:38.964300500+08:00[Asia/Shanghai]")
@Component("com.exiasoft.its.usermgt.endpoint.rest.api.UserApi")
public class UserApi {
    private ApiClient apiClient;

    public UserApi() {
        this(new ApiClient());
    }

    @Autowired
    public UserApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Create user
     * This can only be done by the logged in user.
     * <p><b>201</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>500</b> - Unexpected error occurrs
     * @param createUserRequest User object to create (required)
     * @return User
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public User createUser(CreateUserRequest createUserRequest) throws RestClientException {
        return createUserWithHttpInfo(createUserRequest).getBody();
    }

    /**
     * Create user
     * This can only be done by the logged in user.
     * <p><b>201</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>500</b> - Unexpected error occurrs
     * @param createUserRequest User object to create (required)
     * @return ResponseEntity&lt;User&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<User> createUserWithHttpInfo(CreateUserRequest createUserRequest) throws RestClientException {
        Object postBody = createUserRequest;
        
        // verify the required parameter 'createUserRequest' is set
        if (createUserRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'createUserRequest' when calling createUser");
        }
        
        String path = apiClient.expandPath("/v1/users", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<User> returnType = new ParameterizedTypeReference<User>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Find users
     * Find users by criteria
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input search criteria is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Find user with matched userid (exact match) (optional)
     * @param email Find user with matched email (exact match,  case insensitive) (optional)
     * @param firstName Find user with matched first name (exact match, case insensitive) (optional)
     * @param lastName Find user with matched last name (exact match, case insensitive) (optional)
     * @param limit Maximum number of records to return.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;userid&quot;)
     * @return UserSearchResult
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public UserSearchResult findUser(String userid, String email, String firstName, String lastName, Integer limit, String sort) throws RestClientException {
        return findUserWithHttpInfo(userid, email, firstName, lastName, limit, sort).getBody();
    }

    /**
     * Find users
     * Find users by criteria
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input search criteria is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Find user with matched userid (exact match) (optional)
     * @param email Find user with matched email (exact match,  case insensitive) (optional)
     * @param firstName Find user with matched first name (exact match, case insensitive) (optional)
     * @param lastName Find user with matched last name (exact match, case insensitive) (optional)
     * @param limit Maximum number of records to return.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;userid&quot;)
     * @return ResponseEntity&lt;UserSearchResult&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<UserSearchResult> findUserWithHttpInfo(String userid, String email, String firstName, String lastName, Integer limit, String sort) throws RestClientException {
        Object postBody = null;
        
        String path = apiClient.expandPath("/v1/users", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "userid", userid));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "email", email));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "firstName", firstName));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "lastName", lastName));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "limit", limit));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "sort", sort));

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<UserSearchResult> returnType = new ParameterizedTypeReference<UserSearchResult>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get a single user
     * Get a single user by userid
     * <p><b>200</b> - Successful operation
     * <p><b>403</b> - Input access token is expired
     * <p><b>404</b> - No user is found with input userid
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Id of the user to get (required)
     * @return User
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public User getUser(String userid) throws RestClientException {
        return getUserWithHttpInfo(userid).getBody();
    }

    /**
     * Get a single user
     * Get a single user by userid
     * <p><b>200</b> - Successful operation
     * <p><b>403</b> - Input access token is expired
     * <p><b>404</b> - No user is found with input userid
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Id of the user to get (required)
     * @return ResponseEntity&lt;User&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<User> getUserWithHttpInfo(String userid) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'userid' is set
        if (userid == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'userid' when calling getUser");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("userid", userid);
        String path = apiClient.expandPath("/v1/users/{userid}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<User> returnType = new ParameterizedTypeReference<User>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Update a single user
     * Update a single user by userid
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>404</b> - No user is found with input userid
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Id of the user to update (required)
     * @param user User object to update (required)
     * @return User
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public User putUser(String userid, User user) throws RestClientException {
        return putUserWithHttpInfo(userid, user).getBody();
    }

    /**
     * Update a single user
     * Update a single user by userid
     * <p><b>200</b> - Successful operation
     * <p><b>400</b> - Input information is invalid
     * <p><b>403</b> - Input access token is expired
     * <p><b>404</b> - No user is found with input userid
     * <p><b>500</b> - Unexpected error occurrs
     * @param userid Id of the user to update (required)
     * @param user User object to update (required)
     * @return ResponseEntity&lt;User&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<User> putUserWithHttpInfo(String userid, User user) throws RestClientException {
        Object postBody = user;
        
        // verify the required parameter 'userid' is set
        if (userid == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'userid' when calling putUser");
        }
        
        // verify the required parameter 'user' is set
        if (user == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'user' when calling putUser");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("userid", userid);
        String path = apiClient.expandPath("/v1/users/{userid}", uriVariables);

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap formParams = new LinkedMultiValueMap();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { 
            "application/json"
        };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "bearerAuth" };

        ParameterizedTypeReference<User> returnType = new ParameterizedTypeReference<User>() {};
        return apiClient.invokeAPI(path, HttpMethod.PUT, queryParams, postBody, headerParams, cookieParams, formParams, accept, contentType, authNames, returnType);
    }
}
