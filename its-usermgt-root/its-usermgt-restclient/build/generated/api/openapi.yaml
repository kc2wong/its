openapi: 3.0.2
info:
  description: This is a sample User Management Service based on the OpenAPI 3.0 specification
  title: ITS User Management Service - OpenAPI 3.0
  version: 1.0.0
servers:
- url: http://localhost:8000/usermgt-service
tags:
- description: User Management in ITS System
  name: user
paths:
  /v1/users:
    get:
      description: Find users by criteria
      operationId: findUser
      parameters:
      - description: Find user with matched userid (exact match)
        explode: true
        in: query
        name: userid
        required: false
        schema:
          type: string
        style: form
      - description: Find user with matched email (exact match,  case insensitive)
        explode: true
        in: query
        name: email
        required: false
        schema:
          type: string
        style: form
      - description: Find user with matched first name (exact match, case insensitive)
        explode: true
        in: query
        name: firstName
        required: false
        schema:
          type: string
        style: form
      - description: Find user with matched last name (exact match, case insensitive)
        explode: true
        in: query
        name: lastName
        required: false
        schema:
          type: string
        style: form
      - description: Maximum number of records to return.  Default is 25
        explode: true
        in: query
        name: limit
        required: false
        schema:
          default: 25
          maximum: 200
          type: integer
        style: form
      - description: Comma separated sorting criteria.  To sort in descending order,
          add "-" before the field.  E.g lastName,-email
        example: lastName,-email
        explode: true
        in: query
        name: sort
        required: false
        schema:
          default: userid
          type: string
        style: form
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSearchResult'
          description: Successful operation
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input search criteria is invalid
        "403":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input access token is expired
        "500":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Unexpected error occurrs
      security:
      - bearerAuth: []
      summary: Find users
      tags:
      - user
      x-accepts: application/json
    post:
      description: This can only be done by the logged in user.
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateUserRequest'
        description: User object to create
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
          description: Successful operation
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input information is invalid
        "403":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input access token is expired
        "500":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Unexpected error occurrs
      security:
      - bearerAuth: []
      summary: Create user
      tags:
      - user
      x-contentType: application/json
      x-accepts: application/json
  /v1/users/{userid}:
    get:
      description: Get a single user by userid
      operationId: getUser
      parameters:
      - description: Id of the user to get
        explode: false
        in: path
        name: userid
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
          description: Successful operation
        "403":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input access token is expired
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: No user is found with input userid
        "500":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Unexpected error occurrs
      security:
      - bearerAuth: []
      summary: Get a single user
      tags:
      - user
      x-accepts: application/json
    put:
      description: Update a single user by userid
      operationId: putUser
      parameters:
      - description: Id of the user to update
        explode: false
        in: path
        name: userid
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
        description: User object to update
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
          description: Successful operation
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input information is invalid
        "403":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Input access token is expired
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: No user is found with input userid
        "500":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
          description: Unexpected error occurrs
      security:
      - bearerAuth: []
      summary: Update a single user
      tags:
      - user
      x-contentType: application/json
      x-accepts: application/json
components:
  schemas:
    CreateUserRequest:
      example:
        firstName: John
        lastName: James
        userid: D81040
        email: john.james@gmail.com
      properties:
        userid:
          example: D81040
          maxLength: 20
          minLength: 8
          type: string
        firstName:
          example: John
          maxLength: 100
          type: string
        lastName:
          example: James
          maxLength: 100
          type: string
        email:
          example: john.james@gmail.com
          format: email
          maxLength: 100
          type: string
      required:
      - email
      - firstName
      - lastName
      - userid
      type: object
    User:
      allOf:
      - $ref: '#/components/schemas/CreateUserRequest'
      example:
        updatedBy: d81040
        createdBy: d81040
        createdDateTime: 2017-07-21T17:32:28+08:00
        version: 1
        updatedDateTime: 2017-07-21T17:32:28+08:00
      properties:
        createdBy:
          description: Userid of creator
          example: d81040
          maxLength: 20
          minLength: 8
          type: string
        createdDateTime:
          example: 2017-07-21T17:32:28+08:00
          format: date-time
          type: string
        updatedBy:
          description: Userid of last updater
          example: d81040
          maxLength: 20
          minLength: 8
          type: string
        updatedDateTime:
          example: 2017-07-21T17:32:28+08:00
          format: date-time
          type: string
        version:
          example: 1
          format: int64
          type: number
        status:
          $ref: '#/components/schemas/UserStatus'
      required:
      - createdBy
      - createdDateTime
      - status
      - updatedBy
      - updatedDateTime
      - version
      type: object
    SearchResult:
      properties:
        hasMore:
          description: Indicate there is more record that matches input search criteria
          example: true
          type: boolean
      required:
      - hasMore
      type: object
    PageSearchResult:
      properties:
        start:
          description: Starting record number (from 1) in matched result
          example: 26
          format: int32
          type: integer
        end:
          description: Ending record number in matched result
          example: 50
          format: int32
          type: integer
        total:
          description: Total number of matched record
          example: 256
          format: int32
          type: integer
      type: object
    UserSearchResult:
      allOf:
      - $ref: '#/components/schemas/SearchResult'
      example:
        data:
        - updatedBy: d81040
          createdBy: d81040
          createdDateTime: 2017-07-21T17:32:28+08:00
          version: 1
          updatedDateTime: 2017-07-21T17:32:28+08:00
        - updatedBy: d81040
          createdBy: d81040
          createdDateTime: 2017-07-21T17:32:28+08:00
          version: 1
          updatedDateTime: 2017-07-21T17:32:28+08:00
      properties:
        data:
          items:
            $ref: '#/components/schemas/User'
          type: array
      type: object
    UserStatus:
      description: User Status
      enum:
      - ACTIVE
      - INACTIVE
      - SUSPEND
      type: string
    ErrorResponse:
      properties:
        errorCode:
          description: Error code
          example: ERR-1234
          type: string
        errorParam:
          items:
            example: D81040
            type: string
          type: array
        errorMessage:
          description: Error message in English
          example: Userid D81040 already exists
          type: string
      required:
      - errorCode
      - errorMessage
      type: object
  securitySchemes:
    bearerAuth:
      bearerFormat: JWT
      scheme: bearer
      type: http

