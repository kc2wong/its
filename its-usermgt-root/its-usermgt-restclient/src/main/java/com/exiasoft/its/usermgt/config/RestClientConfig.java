package com.exiasoft.its.usermgt.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.exiasoft.its.usermgt")
public class RestClientConfig {

}