package com.exiasoft.its.usermgt.config

import com.exiasoft.its.common.exception.BaseException
import com.google.firebase.ErrorCode
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
class ApplicationConfig() {

    @PostConstruct
    fun init() {
        BaseException.registerMessageProvider(ErrorCode::class.java)
    }

}