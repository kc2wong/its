package com.exiasoft.its.usermgt.domain.model

import com.exiasoft.its.common.domain.VersionedDomainObject
import java.time.Instant

data class User(val userid: String,
                var firstName: String,
                var lastName: String,
                var email: String) : VersionedDomainObject {
    lateinit var status: UserStatus
    override lateinit var createdBy: String
    override lateinit var createdDateTime: Instant
    override lateinit var updatedBy: String
    override lateinit var updatedDateTime: Instant
    override var version: Long = 1
}