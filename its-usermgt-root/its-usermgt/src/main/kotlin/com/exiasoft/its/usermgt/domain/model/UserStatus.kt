package com.exiasoft.its.usermgt.domain.model

enum class UserStatus(val value: String) {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    SUSPEND("SUSPEND")
}