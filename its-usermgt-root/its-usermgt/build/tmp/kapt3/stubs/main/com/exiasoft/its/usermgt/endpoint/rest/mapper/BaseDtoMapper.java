package com.exiasoft.its.usermgt.endpoint.rest.mapper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u00a8\u0006\t"}, d2 = {"Lcom/exiasoft/its/usermgt/endpoint/rest/mapper/BaseDtoMapper;", "", "()V", "asInstant", "Ljava/time/Instant;", "offsetDateTime", "Ljava/time/OffsetDateTime;", "asOffsetDateTime", "instant", "its-usermgt"})
public final class BaseDtoMapper {
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.OffsetDateTime asOffsetDateTime(@org.jetbrains.annotations.Nullable()
    java.time.Instant instant) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.Instant asInstant(@org.jetbrains.annotations.Nullable()
    java.time.OffsetDateTime offsetDateTime) {
        return null;
    }
    
    public BaseDtoMapper() {
        super();
    }
}