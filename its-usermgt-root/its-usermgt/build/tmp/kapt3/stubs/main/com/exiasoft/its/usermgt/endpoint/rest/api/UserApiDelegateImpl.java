package com.exiasoft.its.usermgt.endpoint.rest.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\b\u0017\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JO\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u00132\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010!J\u0018\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\"\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010$\u001a\u0004\u0018\u00010\u0014H\u0016R\u001c\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u00020\u000b8\u0016@\u0016X\u0097.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006%"}, d2 = {"Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApiDelegateImpl;", "Lcom/exiasoft/its/usermgt/endpoint/rest/api/UserApiDelegate;", "userManagementService", "Lcom/exiasoft/its/usermgt/domain/UserManagementService;", "(Lcom/exiasoft/its/usermgt/domain/UserManagementService;)V", "mapper", "Lcom/exiasoft/its/usermgt/endpoint/rest/mapper/UserDtoMapper;", "kotlin.jvm.PlatformType", "getMapper", "()Lcom/exiasoft/its/usermgt/endpoint/rest/mapper/UserDtoMapper;", "requestData", "Lcom/exiasoft/its/common/bean/RequestData;", "getRequestData", "()Lcom/exiasoft/its/common/bean/RequestData;", "setRequestData", "(Lcom/exiasoft/its/common/bean/RequestData;)V", "getUserManagementService", "()Lcom/exiasoft/its/usermgt/domain/UserManagementService;", "createUser", "Lorg/springframework/http/ResponseEntity;", "Lcom/exiasoft/its/usermgt/endpoint/rest/model/UserDto;", "createUserRequestDto", "Lcom/exiasoft/its/usermgt/endpoint/rest/model/CreateUserRequestDto;", "findUser", "Lcom/exiasoft/its/usermgt/endpoint/rest/model/UserSearchResultDto;", "userid", "", "email", "firstName", "lastName", "limit", "", "sort", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lorg/springframework/http/ResponseEntity;", "getUser", "putUser", "userDto", "its-usermgt"})
@org.springframework.stereotype.Component()
public class UserApiDelegateImpl implements com.exiasoft.its.usermgt.endpoint.rest.api.UserApiDelegate {
    private final com.exiasoft.its.usermgt.endpoint.rest.mapper.UserDtoMapper mapper = null;
    @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    public com.exiasoft.its.common.bean.RequestData requestData;
    @org.jetbrains.annotations.NotNull()
    private final com.exiasoft.its.usermgt.domain.UserManagementService userManagementService = null;
    
    public com.exiasoft.its.usermgt.endpoint.rest.mapper.UserDtoMapper getMapper() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.exiasoft.its.common.bean.RequestData getRequestData() {
        return null;
    }
    
    public void setRequestData(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.bean.RequestData p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.usermgt.endpoint.rest.model.UserDto> createUser(@org.jetbrains.annotations.Nullable()
    com.exiasoft.its.usermgt.endpoint.rest.model.CreateUserRequestDto createUserRequestDto) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.usermgt.endpoint.rest.model.UserSearchResultDto> findUser(@org.jetbrains.annotations.Nullable()
    java.lang.String userid, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String firstName, @org.jetbrains.annotations.Nullable()
    java.lang.String lastName, @org.jetbrains.annotations.Nullable()
    java.lang.Integer limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.usermgt.endpoint.rest.model.UserDto> getUser(@org.jetbrains.annotations.Nullable()
    java.lang.String userid) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.http.ResponseEntity<com.exiasoft.its.usermgt.endpoint.rest.model.UserDto> putUser(@org.jetbrains.annotations.Nullable()
    java.lang.String userid, @org.jetbrains.annotations.Nullable()
    com.exiasoft.its.usermgt.endpoint.rest.model.UserDto userDto) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.exiasoft.its.usermgt.domain.UserManagementService getUserManagementService() {
        return null;
    }
    
    public UserApiDelegateImpl(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.UserManagementService userManagementService) {
        super();
    }
}