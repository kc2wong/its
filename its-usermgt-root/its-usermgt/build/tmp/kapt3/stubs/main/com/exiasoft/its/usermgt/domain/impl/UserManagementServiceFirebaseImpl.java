package com.exiasoft.its.usermgt.domain.impl;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0017\u0018\u0000 +2\u00020\u0001:\u0001+B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0012JT\u0010\u0016\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u0018\u0012\u0004\u0012\u00020\u00190\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\u001bH\u0016J\u0012\u0010\"\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0012\u0010#\u001a\u0004\u0018\u00010$2\u0006\u0010\u001c\u001a\u00020\u001bH\u0012J\u0012\u0010%\u001a\u0004\u0018\u00010$2\u0006\u0010&\u001a\u00020\u001bH\u0012J\u0018\u0010\'\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u001c\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020*0)2\u0006\u0010\u0010\u001a\u00020\u000fH\u0012R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lcom/exiasoft/its/usermgt/domain/impl/UserManagementServiceFirebaseImpl;", "Lcom/exiasoft/its/usermgt/domain/UserManagementService;", "firebaseAuth", "Lcom/google/firebase/auth/FirebaseAuth;", "fireStore", "Lcom/google/cloud/firestore/Firestore;", "(Lcom/google/firebase/auth/FirebaseAuth;Lcom/google/cloud/firestore/Firestore;)V", "getFireStore", "()Lcom/google/cloud/firestore/Firestore;", "getFirebaseAuth", "()Lcom/google/firebase/auth/FirebaseAuth;", "logger", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "createUser", "Lcom/exiasoft/its/usermgt/domain/model/User;", "user", "invocationUser", "Lcom/exiasoft/its/common/domain/UserProfile;", "documentToUser", "documentSnapshot", "Lcom/google/cloud/firestore/DocumentSnapshot;", "findUser", "Lkotlin/Pair;", "", "", "userId", "", "email", "firstName", "lastName", "limit", "", "sort", "getUser", "getUserByEmail", "Lcom/google/firebase/auth/UserRecord;", "getUserByUserid", "userid", "updateUser", "userToUserData", "", "", "Companion", "its-usermgt"})
@org.springframework.stereotype.Service()
public class UserManagementServiceFirebaseImpl implements com.exiasoft.its.usermgt.domain.UserManagementService {
    private final org.slf4j.Logger logger = null;
    @org.jetbrains.annotations.NotNull()
    private final com.google.firebase.auth.FirebaseAuth firebaseAuth = null;
    @org.jetbrains.annotations.NotNull()
    private final com.google.cloud.firestore.Firestore fireStore = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FB_ERROR_CODE_USER_NOT_FOUND = "USER_NOT_FOUND";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FB_ERROR_CODE_UID_ALREADY_EXISTS = "UID_ALREADY_EXISTS";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FB_ERROR_CODE_EMAIL_ALREADY_EXISTS = "EMAIL_ALREADY_EXISTS";
    public static final com.exiasoft.its.usermgt.domain.impl.UserManagementServiceFirebaseImpl.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.exiasoft.its.usermgt.domain.model.User createUser(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.User user, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invocationUser) throws com.exiasoft.its.common.exception.ValidationException, com.exiasoft.its.common.exception.SystemException {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.exiasoft.its.usermgt.domain.model.User updateUser(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.User user, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invocationUser) throws com.exiasoft.its.common.exception.ValidationException, com.exiasoft.its.common.exception.SystemException {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.exiasoft.its.usermgt.domain.model.User getUser(@org.jetbrains.annotations.NotNull()
    java.lang.String userId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlin.Pair<java.util.List<com.exiasoft.its.usermgt.domain.model.User>, java.lang.Boolean> findUser(@org.jetbrains.annotations.Nullable()
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String firstName, @org.jetbrains.annotations.Nullable()
    java.lang.String lastName, int limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort) {
        return null;
    }
    
    private com.google.firebase.auth.UserRecord getUserByEmail(java.lang.String email) throws com.exiasoft.its.common.exception.SystemException {
        return null;
    }
    
    private com.google.firebase.auth.UserRecord getUserByUserid(java.lang.String userid) throws com.exiasoft.its.common.exception.SystemException {
        return null;
    }
    
    private java.util.Map<java.lang.String, java.lang.Object> userToUserData(com.exiasoft.its.usermgt.domain.model.User user) {
        return null;
    }
    
    private com.exiasoft.its.usermgt.domain.model.User documentToUser(com.google.cloud.firestore.DocumentSnapshot documentSnapshot) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.google.firebase.auth.FirebaseAuth getFirebaseAuth() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.google.cloud.firestore.Firestore getFireStore() {
        return null;
    }
    
    public UserManagementServiceFirebaseImpl(@org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.google.firebase.auth.FirebaseAuth firebaseAuth, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    com.google.cloud.firestore.Firestore fireStore) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/exiasoft/its/usermgt/domain/impl/UserManagementServiceFirebaseImpl$Companion;", "", "()V", "FB_ERROR_CODE_EMAIL_ALREADY_EXISTS", "", "FB_ERROR_CODE_UID_ALREADY_EXISTS", "FB_ERROR_CODE_USER_NOT_FOUND", "its-usermgt"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}