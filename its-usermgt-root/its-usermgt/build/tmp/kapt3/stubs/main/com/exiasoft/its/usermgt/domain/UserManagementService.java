package com.exiasoft.its.usermgt.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&JT\u0010\u0007\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\t\u0012\u0004\u0012\u00020\n0\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\fH&J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\fH&J\u0018\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0015"}, d2 = {"Lcom/exiasoft/its/usermgt/domain/UserManagementService;", "", "createUser", "Lcom/exiasoft/its/usermgt/domain/model/User;", "user", "invocationUser", "Lcom/exiasoft/its/common/domain/UserProfile;", "findUser", "Lkotlin/Pair;", "", "", "userId", "", "email", "firstName", "lastName", "limit", "", "sort", "getUser", "updateUser", "its-usermgt"})
public abstract interface UserManagementService {
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.exiasoft.its.usermgt.domain.model.User getUser(@org.jetbrains.annotations.NotNull()
    java.lang.String userId);
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlin.Pair<java.util.List<com.exiasoft.its.usermgt.domain.model.User>, java.lang.Boolean> findUser(@org.jetbrains.annotations.Nullable()
    java.lang.String userId, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String firstName, @org.jetbrains.annotations.Nullable()
    java.lang.String lastName, int limit, @org.jetbrains.annotations.Nullable()
    java.lang.String sort);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.exiasoft.its.usermgt.domain.model.User createUser(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.User user, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invocationUser);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.exiasoft.its.usermgt.domain.model.User updateUser(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.User user, @org.jetbrains.annotations.NotNull()
    com.exiasoft.its.common.domain.UserProfile invocationUser);
}