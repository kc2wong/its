package com.exiasoft.its.usermgt.endpoint.rest.mapper;

import java.lang.System;

@org.mapstruct.Mapper(uses = {com.exiasoft.its.usermgt.endpoint.rest.mapper.BaseDtoMapper.class})
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0002\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0017J\u0010\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0003H\'J\u0010\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u0006H\u0017\u00a8\u0006\f"}, d2 = {"Lcom/exiasoft/its/usermgt/endpoint/rest/mapper/UserDtoMapper;", "", "domain2Dto", "Lcom/exiasoft/its/usermgt/endpoint/rest/model/UserDto;", "user", "Lcom/exiasoft/its/usermgt/domain/model/User;", "Lcom/exiasoft/its/usermgt/endpoint/rest/model/UserStatusDto;", "userStatus", "Lcom/exiasoft/its/usermgt/domain/model/UserStatus;", "dto2Domain", "userDto", "userStatusDto", "its-usermgt"})
public abstract interface UserDtoMapper {
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "status", source = "status", qualifiedByName = {"toUserStatus"})})
    public abstract com.exiasoft.its.usermgt.domain.model.User dto2Domain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.endpoint.rest.model.UserDto userDto);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Mappings(value = {@org.mapstruct.Mapping(target = "status", source = "status", qualifiedByName = {"toUserStatusDto"})})
    public abstract com.exiasoft.its.usermgt.endpoint.rest.model.UserDto domain2Dto(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.User user);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Named(value = "toUserStatus")
    public abstract com.exiasoft.its.usermgt.domain.model.UserStatus dto2Domain(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto userStatusDto);
    
    @org.jetbrains.annotations.NotNull()
    @org.mapstruct.Named(value = "toUserStatusDto")
    public abstract com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto domain2Dto(@org.jetbrains.annotations.NotNull()
    com.exiasoft.its.usermgt.domain.model.UserStatus userStatus);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        @org.jetbrains.annotations.NotNull()
        @org.mapstruct.Named(value = "toUserStatus")
        public static com.exiasoft.its.usermgt.domain.model.UserStatus dto2Domain(com.exiasoft.its.usermgt.endpoint.rest.mapper.UserDtoMapper $this, @org.jetbrains.annotations.NotNull()
        com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto userStatusDto) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @org.mapstruct.Named(value = "toUserStatusDto")
        public static com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto domain2Dto(com.exiasoft.its.usermgt.endpoint.rest.mapper.UserDtoMapper $this, @org.jetbrains.annotations.NotNull()
        com.exiasoft.its.usermgt.domain.model.UserStatus userStatus) {
            return null;
        }
    }
}