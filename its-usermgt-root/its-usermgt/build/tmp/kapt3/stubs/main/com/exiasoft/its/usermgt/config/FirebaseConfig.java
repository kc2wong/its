package com.exiasoft.its.usermgt.config;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\r\u001a\u00020\u000eH\u0017J\b\u0010\u000f\u001a\u00020\u0010H\u0017J\b\u0010\u0011\u001a\u00020\u0012H\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0006\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u000e\u0010\u000b\u001a\u00020\fX\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/exiasoft/its/usermgt/config/FirebaseConfig;", "", "applicationContext", "Lorg/springframework/context/ApplicationContext;", "firebaseDatabaseUrl", "", "firebaseServiceAccountCredential", "(Lorg/springframework/context/ApplicationContext;Ljava/lang/String;Ljava/lang/String;)V", "getFirebaseDatabaseUrl", "()Ljava/lang/String;", "getFirebaseServiceAccountCredential", "logger", "Lorg/slf4j/Logger;", "fireStore", "Lcom/google/cloud/firestore/Firestore;", "firebaseAuth", "Lcom/google/firebase/auth/FirebaseAuth;", "firebaseInit", "", "its-usermgt"})
@org.springframework.context.annotation.Configuration()
public class FirebaseConfig {
    private final org.slf4j.Logger logger = null;
    private final org.springframework.context.ApplicationContext applicationContext = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String firebaseDatabaseUrl = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String firebaseServiceAccountCredential = null;
    
    @javax.annotation.PostConstruct()
    public void firebaseInit() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.context.annotation.Bean()
    public com.google.firebase.auth.FirebaseAuth firebaseAuth() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.context.annotation.Bean()
    public com.google.cloud.firestore.Firestore fireStore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public java.lang.String getFirebaseDatabaseUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public java.lang.String getFirebaseServiceAccountCredential() {
        return null;
    }
    
    public FirebaseConfig(@org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Autowired()
    org.springframework.context.ApplicationContext applicationContext, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Value(value = "${firebase.database.url}")
    java.lang.String firebaseDatabaseUrl, @org.jetbrains.annotations.NotNull()
    @org.springframework.beans.factory.annotation.Value(value = "${firebase.service.account.credential}")
    java.lang.String firebaseServiceAccountCredential) {
        super();
    }
}