package com.exiasoft.its.usermgt.endpoint.rest.mapper;

import com.exiasoft.its.usermgt.domain.model.User;
import com.exiasoft.its.usermgt.domain.model.UserStatus;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserDto;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto;
import java.math.BigDecimal;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-12T23:05:55+0800",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from kotlin-annotation-processing-gradle-1.3.72.jar, environment: Java 11.0.8 (Oracle Corporation)"
)
public class UserDtoMapperImpl implements UserDtoMapper {

    private final BaseDtoMapper baseDtoMapper = new BaseDtoMapper();

    @Override
    public User dto2Domain(UserDto userDto) {
        if ( userDto == null ) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        String email = null;
        String userid = null;

        firstName = userDto.getFirstName();
        lastName = userDto.getLastName();
        email = userDto.getEmail();
        userid = userDto.getUserid();

        User user = new User( userid, firstName, lastName, email );

        user.setStatus( dto2Domain( userDto.getStatus() ) );
        user.setCreatedBy( userDto.getCreatedBy() );
        user.setCreatedDateTime( baseDtoMapper.asInstant( userDto.getCreatedDateTime() ) );
        user.setUpdatedBy( userDto.getUpdatedBy() );
        user.setUpdatedDateTime( baseDtoMapper.asInstant( userDto.getUpdatedDateTime() ) );
        if ( userDto.getVersion() != null ) {
            user.setVersion( userDto.getVersion().longValue() );
        }

        return user;
    }

    @Override
    public UserDto domain2Dto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setStatus( domain2Dto( user.getStatus() ) );
        userDto.setUserid( user.getUserid() );
        userDto.setFirstName( user.getFirstName() );
        userDto.setLastName( user.getLastName() );
        userDto.setEmail( user.getEmail() );
        userDto.setCreatedBy( user.getCreatedBy() );
        userDto.setCreatedDateTime( baseDtoMapper.asOffsetDateTime( user.getCreatedDateTime() ) );
        userDto.setUpdatedBy( user.getUpdatedBy() );
        userDto.setUpdatedDateTime( baseDtoMapper.asOffsetDateTime( user.getUpdatedDateTime() ) );
        userDto.setVersion( BigDecimal.valueOf( user.getVersion() ) );

        return userDto;
    }

    @Override
    public UserStatus dto2Domain(UserStatusDto userStatusDto) {
        if ( userStatusDto == null ) {
            return null;
        }

        UserStatus userStatus;

        switch ( userStatusDto ) {
            case ACTIVE: userStatus = UserStatus.ACTIVE;
            break;
            case INACTIVE: userStatus = UserStatus.INACTIVE;
            break;
            case SUSPEND: userStatus = UserStatus.SUSPEND;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + userStatusDto );
        }

        return userStatus;
    }

    @Override
    public UserStatusDto domain2Dto(UserStatus userStatus) {
        if ( userStatus == null ) {
            return null;
        }

        UserStatusDto userStatusDto;

        switch ( userStatus ) {
            case ACTIVE: userStatusDto = UserStatusDto.ACTIVE;
            break;
            case INACTIVE: userStatusDto = UserStatusDto.INACTIVE;
            break;
            case SUSPEND: userStatusDto = UserStatusDto.SUSPEND;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + userStatus );
        }

        return userStatusDto;
    }
}
