package com.exiasoft.its.usermgt.endpoint.rest.model;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * User Status
 */
public enum UserStatusDto {
  
  ACTIVE("ACTIVE"),
  
  INACTIVE("INACTIVE"),
  
  SUSPEND("SUSPEND");

  private String value;

  UserStatusDto(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static UserStatusDto fromValue(String value) {
    for (UserStatusDto b : UserStatusDto.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

