package com.exiasoft.its.usermgt.endpoint.rest.model;

import java.util.Objects;
import com.exiasoft.its.usermgt.endpoint.rest.model.CreateUserRequestDto;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserStatusDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T23:05:52.379700600+08:00[Asia/Shanghai]")

public class UserDto extends CreateUserRequestDto  {
  @JsonProperty("createdBy")
  private String createdBy;

  @JsonProperty("createdDateTime")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime createdDateTime;

  @JsonProperty("updatedBy")
  private String updatedBy;

  @JsonProperty("updatedDateTime")
  @org.springframework.format.annotation.DateTimeFormat(iso = org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime updatedDateTime;

  @JsonProperty("version")
  private BigDecimal version;

  @JsonProperty("status")
  private UserStatusDto status;

  public UserDto createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  /**
   * Userid of creator
   * @return createdBy
  */
  @ApiModelProperty(example = "d81040", required = true, value = "Userid of creator")
  @NotNull

@Size(min=8,max=20) 
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public UserDto createdDateTime(OffsetDateTime createdDateTime) {
    this.createdDateTime = createdDateTime;
    return this;
  }

  /**
   * Get createdDateTime
   * @return createdDateTime
  */
  @ApiModelProperty(example = "2017-07-21T17:32:28+08:00", required = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getCreatedDateTime() {
    return createdDateTime;
  }

  public void setCreatedDateTime(OffsetDateTime createdDateTime) {
    this.createdDateTime = createdDateTime;
  }

  public UserDto updatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  /**
   * Userid of last updater
   * @return updatedBy
  */
  @ApiModelProperty(example = "d81040", required = true, value = "Userid of last updater")
  @NotNull

@Size(min=8,max=20) 
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public UserDto updatedDateTime(OffsetDateTime updatedDateTime) {
    this.updatedDateTime = updatedDateTime;
    return this;
  }

  /**
   * Get updatedDateTime
   * @return updatedDateTime
  */
  @ApiModelProperty(example = "2017-07-21T17:32:28+08:00", required = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getUpdatedDateTime() {
    return updatedDateTime;
  }

  public void setUpdatedDateTime(OffsetDateTime updatedDateTime) {
    this.updatedDateTime = updatedDateTime;
  }

  public UserDto version(BigDecimal version) {
    this.version = version;
    return this;
  }

  /**
   * Get version
   * @return version
  */
  @ApiModelProperty(example = "1", required = true, value = "")
  @NotNull

  @Valid

  public BigDecimal getVersion() {
    return version;
  }

  public void setVersion(BigDecimal version) {
    this.version = version;
  }

  public UserDto status(UserStatusDto status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public UserStatusDto getStatus() {
    return status;
  }

  public void setStatus(UserStatusDto status) {
    this.status = status;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDto user = (UserDto) o;
    return Objects.equals(this.createdBy, user.createdBy) &&
        Objects.equals(this.createdDateTime, user.createdDateTime) &&
        Objects.equals(this.updatedBy, user.updatedBy) &&
        Objects.equals(this.updatedDateTime, user.updatedDateTime) &&
        Objects.equals(this.version, user.version) &&
        Objects.equals(this.status, user.status) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(createdBy, createdDateTime, updatedBy, updatedDateTime, version, status, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserDto {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    createdDateTime: ").append(toIndentedString(createdDateTime)).append("\n");
    sb.append("    updatedBy: ").append(toIndentedString(updatedBy)).append("\n");
    sb.append("    updatedDateTime: ").append(toIndentedString(updatedDateTime)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

