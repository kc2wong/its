package com.exiasoft.its.usermgt.endpoint.rest.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ErrorResponseDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T23:05:52.379700600+08:00[Asia/Shanghai]")

public class ErrorResponseDto   {
  @JsonProperty("errorCode")
  private String errorCode;

  @JsonProperty("errorParam")
  @Valid
  private List<String> errorParam = null;

  @JsonProperty("errorMessage")
  private String errorMessage;

  public ErrorResponseDto errorCode(String errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  /**
   * Error code
   * @return errorCode
  */
  @ApiModelProperty(example = "ERR-1234", required = true, value = "Error code")
  @NotNull


  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public ErrorResponseDto errorParam(List<String> errorParam) {
    this.errorParam = errorParam;
    return this;
  }

  public ErrorResponseDto addErrorParamItem(String errorParamItem) {
    if (this.errorParam == null) {
      this.errorParam = new ArrayList<>();
    }
    this.errorParam.add(errorParamItem);
    return this;
  }

  /**
   * Get errorParam
   * @return errorParam
  */
  @ApiModelProperty(value = "")


  public List<String> getErrorParam() {
    return errorParam;
  }

  public void setErrorParam(List<String> errorParam) {
    this.errorParam = errorParam;
  }

  public ErrorResponseDto errorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

  /**
   * Error message in English
   * @return errorMessage
  */
  @ApiModelProperty(example = "Userid D81040 already exists", required = true, value = "Error message in English")
  @NotNull


  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorResponseDto errorResponse = (ErrorResponseDto) o;
    return Objects.equals(this.errorCode, errorResponse.errorCode) &&
        Objects.equals(this.errorParam, errorResponse.errorParam) &&
        Objects.equals(this.errorMessage, errorResponse.errorMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(errorCode, errorParam, errorMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorResponseDto {\n");
    
    sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
    sb.append("    errorParam: ").append(toIndentedString(errorParam)).append("\n");
    sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

