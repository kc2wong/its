package com.exiasoft.its.usermgt.endpoint.rest.api;

import com.exiasoft.its.usermgt.endpoint.rest.model.CreateUserRequestDto;
import com.exiasoft.its.usermgt.endpoint.rest.model.ErrorResponseDto;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserDto;
import com.exiasoft.its.usermgt.endpoint.rest.model.UserSearchResultDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UserApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T23:05:52.379700600+08:00[Asia/Shanghai]")

public interface UserApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /v1/users : Create user
     * This can only be done by the logged in user.
     *
     * @param createUserRequestDto User object to create (required)
     * @return Successful operation (status code 201)
     *         or Input information is invalid (status code 400)
     *         or Input access token is expired (status code 403)
     *         or Unexpected error occurrs (status code 500)
     * @see UserApi#createUser
     */
    default ResponseEntity<UserDto> createUser(CreateUserRequestDto createUserRequestDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"updatedBy\" : \"d81040\", \"createdBy\" : \"d81040\", \"createdDateTime\" : \"2017-07-21T17:32:28+08:00\", \"version\" : 1, \"updatedDateTime\" : \"2017-07-21T17:32:28+08:00\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /v1/users : Find users
     * Find users by criteria
     *
     * @param userid Find user with matched userid (exact match) (optional)
     * @param email Find user with matched email (exact match,  case insensitive) (optional)
     * @param firstName Find user with matched first name (exact match, case insensitive) (optional)
     * @param lastName Find user with matched last name (exact match, case insensitive) (optional)
     * @param limit Maximum number of records to return.  Default is 25 (optional, default to 25)
     * @param sort Comma separated sorting criteria.  To sort in descending order, add \&quot;-\&quot; before the field.  E.g lastName,-email (optional, default to &quot;userid&quot;)
     * @return Successful operation (status code 200)
     *         or Input search criteria is invalid (status code 400)
     *         or Input access token is expired (status code 403)
     *         or Unexpected error occurrs (status code 500)
     * @see UserApi#findUser
     */
    default ResponseEntity<UserSearchResultDto> findUser(String userid,
        String email,
        String firstName,
        String lastName,
        Integer limit,
        String sort) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"data\" : [ { \"updatedBy\" : \"d81040\", \"createdBy\" : \"d81040\", \"createdDateTime\" : \"2017-07-21T17:32:28+08:00\", \"version\" : 1, \"updatedDateTime\" : \"2017-07-21T17:32:28+08:00\" }, { \"updatedBy\" : \"d81040\", \"createdBy\" : \"d81040\", \"createdDateTime\" : \"2017-07-21T17:32:28+08:00\", \"version\" : 1, \"updatedDateTime\" : \"2017-07-21T17:32:28+08:00\" } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /v1/users/{userid} : Get a single user
     * Get a single user by userid
     *
     * @param userid Id of the user to get (required)
     * @return Successful operation (status code 200)
     *         or Input access token is expired (status code 403)
     *         or No user is found with input userid (status code 404)
     *         or Unexpected error occurrs (status code 500)
     * @see UserApi#getUser
     */
    default ResponseEntity<UserDto> getUser(String userid) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"updatedBy\" : \"d81040\", \"createdBy\" : \"d81040\", \"createdDateTime\" : \"2017-07-21T17:32:28+08:00\", \"version\" : 1, \"updatedDateTime\" : \"2017-07-21T17:32:28+08:00\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * PUT /v1/users/{userid} : Update a single user
     * Update a single user by userid
     *
     * @param userid Id of the user to update (required)
     * @param userDto User object to update (required)
     * @return Successful operation (status code 200)
     *         or Input information is invalid (status code 400)
     *         or Input access token is expired (status code 403)
     *         or No user is found with input userid (status code 404)
     *         or Unexpected error occurrs (status code 500)
     * @see UserApi#putUser
     */
    default ResponseEntity<UserDto> putUser(String userid,
        UserDto userDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"updatedBy\" : \"d81040\", \"createdBy\" : \"d81040\", \"createdDateTime\" : \"2017-07-21T17:32:28+08:00\", \"version\" : 1, \"updatedDateTime\" : \"2017-07-21T17:32:28+08:00\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
