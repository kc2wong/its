package com.exiasoft.its.usermgt.endpoint.rest.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PageSearchResultDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-11-12T23:05:52.379700600+08:00[Asia/Shanghai]")

public class PageSearchResultDto   {
  @JsonProperty("start")
  private Integer start;

  @JsonProperty("end")
  private Integer end;

  @JsonProperty("total")
  private Integer total;

  public PageSearchResultDto start(Integer start) {
    this.start = start;
    return this;
  }

  /**
   * Starting record number (from 1) in matched result
   * @return start
  */
  @ApiModelProperty(example = "26", value = "Starting record number (from 1) in matched result")


  public Integer getStart() {
    return start;
  }

  public void setStart(Integer start) {
    this.start = start;
  }

  public PageSearchResultDto end(Integer end) {
    this.end = end;
    return this;
  }

  /**
   * Ending record number in matched result
   * @return end
  */
  @ApiModelProperty(example = "50", value = "Ending record number in matched result")


  public Integer getEnd() {
    return end;
  }

  public void setEnd(Integer end) {
    this.end = end;
  }

  public PageSearchResultDto total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Total number of matched record
   * @return total
  */
  @ApiModelProperty(example = "256", value = "Total number of matched record")


  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PageSearchResultDto pageSearchResult = (PageSearchResultDto) o;
    return Objects.equals(this.start, pageSearchResult.start) &&
        Objects.equals(this.end, pageSearchResult.end) &&
        Objects.equals(this.total, pageSearchResult.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(start, end, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PageSearchResultDto {\n");
    
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

